import os
import re

from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(dir_path, "VERSION"), "r") as version_file:
    version = str(version_file.readline()).strip()

package_name = "lara_django_base"
package_data = {package_name: [
    "fixtures/*.json", f"templates/{package_name}/*.html", "templates/registration/*.html",
    "static/css/*.css", "static/sass/*.scss", "static/js/*.js", "static/icons/*.svg", "static/images/*.ico", ]}

setup(
    name=package_name,
    version=version,
    packages=find_packages(),
    include_package_data=True,
    package_data=package_data,
)
