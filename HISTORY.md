
# History  of lara_django_base 


* 0.2.10 (2023-04-24)


* First release ....

v0.2.40-1-g018241f
------------------

Features:

 - 018241f New standard data types added


v0.2.41
-------

Features:

 - 018241f New standard data types added

Bug fixes:

 - a4a97cf Fixutures, models, api

Other changes:

 - e22c305 Bump version: 0.2.40 → 0.2.41

v0.2.42
-------

Bug fixes:

 - 74e4785 HISTORY.md
 - 29e7b9a Setuptools and wheels added to .gitlab-ci.yml

Other changes:

 - fcb0a9a Bump version: 0.2.41 → 0.2.42

v0.2.46
-------

Features:

 - 70468bf Filters added; namespace validation deactivated temp.

Other changes:

 - e4c623c Bump version: 0.2.45 → 0.2.46

v0.2.47
-------

Bug fixes:

 - 8741b73 Domain uniqueness removed
 - 760a2f8 GRPC API updated
 - f153474 HISTORY.md

Other changes:

 - f44be7a Bump version: 0.2.46 → 0.2.47

v0.2.48
-------

Features:

 - cc759c2 Fixtures for tags, physical units, scientific constants, itemstatus and room categories added
 - 63e3fb0 Itemstatus fixture added; IRIs added

Bug fixes:

 - 5d5035a GRPC API updated
 - a59ab56 HISTORY.md

Other changes:

 - c035117 Bump version: 0.2.47 → 0.2.48

v0.2.49
-------

Bug fixes:

 - a805308 Forms.py
 - 2fcd379 GRPC API updated
 - 7a8a27c HISTORY.md
 - 09a61d4 Literature->resources_external; handle->PID; create & update view; filters.py

Other changes:

 - 981e07b Bump version: 0.2.48 → 0.2.49

v0.2.50
-------

Features:

 - ce14c05 Models.py prefix adde; fix scientific constants fixtures; fix datatype fixtures

Bug fixes:

 - b56f922 HISTORY.md

Other changes:

 - 85a0547 Bump version: 0.2.49 → 0.2.50

v0.2.51
-------

Features:

 - 88aca49 New template structure
 - aa2a4b4 URN field with validator added

Bug fixes:

 - 7639473 GRPC API updated
 - 99cc09e GRPC API updated
 - a222c03 GRPC API updated
 - c24d1b3 GRPC API updated
 - da3f601 HISTORY.md

Other changes:

 - 71be85e Bump version: 0.2.50 → 0.2.51

v0.2.53
-------

Bug fixes:

 - 49eb8fe Static files copied back

Other changes:

 - b949b5c Bump version: 0.2.52 → 0.2.53

v0.2.54
-------

Bug fixes:

 - 3273789 GRPC API updated
 - 3f79fe0 HISTORY.md
 - afe2ecb LARAServerProtoSerializer tag serialisation added
 - 8e7f2a2 New gRPC proto files generated

Other changes:

 - 1296fae Bump version: 0.2.53 → 0.2.54

v0.2.55
-------

Features:

 - 2b25f65 First blockchain hash model added; datetime_last_modified added; SyncStatusAbstr added
 - efe7a76 LARA logo added
 - 8127e60 New, modular homepage template structure
 - 65f3cf0 New tools and admin menu added

Bug fixes:

 - a3c7dd6 GRPC API updated
 - 8030d76 HISTORY.md

Other changes:

 - bdad9f4 Bump version: 0.2.54 → 0.2.55

v0.2.56
-------

Bug fixes:

 - 26e5433 Dataetime references
 - 89dc512 Dataetime references
 - a456bf2 GRPC API updated
 - 79981d3 HISTORY.md
 - 315fcae URN generation with //

Other changes:

 - a261977 Bump version: 0.2.55 → 0.2.56

v0.2.57
-------

Bug fixes:

 - 9565e4b GRPC API updated
 - aced1e2 HISTORY.md

Other changes:

 - ae62487 Bump version: 0.2.56 → 0.2.57

v0.2.58
-------

Bug fixes:

 - 860708b HISTORY.md
 - d300603 Static files moved to subfolder

Other changes:

 - 81a7a07 Bump version: 0.2.57 → 0.2.58

v0.2.59
-------

Bug fixes:

 - 76ce68a All filtersets activated
 - 8ab74ef GRPC API updated
 - fc733be HISTORY.md
 - 192f617 Improved filters / search / api
 - 68b9576 Models / Namespace : URN -> URI, ExternalResource: URN: IRI Location: URI removed; IRI generators added; improved filtersets

Other changes:

 - d51f724 Bump version: 0.2.58 → 0.2.59

