# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc
import  lara_service_pb2 as lara__service__pb2


class LaraServiceStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.ServiceName = channel.unary_unary(
        '/org.larasuite.lara_django_service.LaraService/ServiceName',
        request_serializer=lara__service__pb2.Empty.SerializeToString,
        response_deserializer=lara__service__pb2.ServiceNameResponse.FromString,
        )


class LaraServiceServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def ServiceName(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_LaraServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'ServiceName': grpc.unary_unary_rpc_method_handler(
          servicer.ServiceName,
          request_deserializer=lara__service__pb2.Empty.FromString,
          response_serializer=lara__service__pb2.ServiceNameResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'org.larasuite.lara_django_service.LaraService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
