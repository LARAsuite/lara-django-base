# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: lara_service.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='lara_service.proto',
  package='org.larasuite.lara_django_service',
  syntax='proto3',
  serialized_options=b'B\021LaraDjangoService',
  serialized_pb=b'\n\x12lara_service.proto\x12!org.larasuite.lara_django_service\"\x07\n\x05\x45mpty\"#\n\x13ServiceNameResponse\x12\x0c\n\x04name\x18\x01 \x01(\t2\x80\x01\n\x0bLaraService\x12q\n\x0bServiceName\x12(.org.larasuite.lara_django_service.Empty\x1a\x36.org.larasuite.lara_django_service.ServiceNameResponse\"\x00\x42\x13\x42\x11LaraDjangoServiceb\x06proto3'
)




_EMPTY = _descriptor.Descriptor(
  name='Empty',
  full_name='org.larasuite.lara_django_service.Empty',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=57,
  serialized_end=64,
)


_SERVICENAMERESPONSE = _descriptor.Descriptor(
  name='ServiceNameResponse',
  full_name='org.larasuite.lara_django_service.ServiceNameResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='org.larasuite.lara_django_service.ServiceNameResponse.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=66,
  serialized_end=101,
)

DESCRIPTOR.message_types_by_name['Empty'] = _EMPTY
DESCRIPTOR.message_types_by_name['ServiceNameResponse'] = _SERVICENAMERESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Empty = _reflection.GeneratedProtocolMessageType('Empty', (_message.Message,), {
  'DESCRIPTOR' : _EMPTY,
  '__module__' : 'lara_service_pb2'
  # @@protoc_insertion_point(class_scope:org.larasuite.lara_django_service.Empty)
  })
_sym_db.RegisterMessage(Empty)

ServiceNameResponse = _reflection.GeneratedProtocolMessageType('ServiceNameResponse', (_message.Message,), {
  'DESCRIPTOR' : _SERVICENAMERESPONSE,
  '__module__' : 'lara_service_pb2'
  # @@protoc_insertion_point(class_scope:org.larasuite.lara_django_service.ServiceNameResponse)
  })
_sym_db.RegisterMessage(ServiceNameResponse)


DESCRIPTOR._options = None

_LARASERVICE = _descriptor.ServiceDescriptor(
  name='LaraService',
  full_name='org.larasuite.lara_django_service.LaraService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=104,
  serialized_end=232,
  methods=[
  _descriptor.MethodDescriptor(
    name='ServiceName',
    full_name='org.larasuite.lara_django_service.LaraService.ServiceName',
    index=0,
    containing_service=None,
    input_type=_EMPTY,
    output_type=_SERVICENAMERESPONSE,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_LARASERVICE)

DESCRIPTOR.services_by_name['LaraService'] = _LARASERVICE

# @@protoc_insertion_point(module_scope)
