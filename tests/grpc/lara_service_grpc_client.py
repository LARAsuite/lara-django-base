import logging

import grpc

import lara_service_pb2
import lara_service_pb2_grpc

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = lara_service_pb2_grpc.LaraServiceStub(channel)
        response = stub.ServiceName(lara_service_pb2.Empty())
    print("Server name received: " + response.name )


if __name__ == '__main__':
    logging.basicConfig()
    run()