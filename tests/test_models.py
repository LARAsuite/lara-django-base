"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base tests *

:details: lara_django_base application models tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

# from lara_django_base.models import

# Create your lara_django_base tests here.


class ModelsTests(TestCase):
    def test_Addresses(self):
        test_city = City.objects.create()
        test_subdivision = CountrySubdivision.objects.create()
        test_country = Country.objects.create()
        test_geolocation = GeoLocation.objects.create()

        new_addr = Address.objects.create(building="B1", street="TestStreet", house_number=3, supplement="left",
                                          city=test_city, subdivision=test_subdivision, country=test_country, postal_code="XXY",
                                          geolocation=test_geolocation)

        self.assertEqual(new_addr.postal_code, "XXY")
