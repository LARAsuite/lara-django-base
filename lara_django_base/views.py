"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base views *

:details: lara_django_base views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List
from configparser import ConfigParser, NoSectionError, NoOptionError
from distutils.log import debug
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django import __version__ as django_version
import environ

from django_tables2 import SingleTableView

from .models import Address

from .forms import AddressCreateForm, AddressUpdateForm
from .tables import AddressTable

# Create your  lara_django_base views here.


@dataclass
class ItemMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [
        {'name': 'item1',
         'path': 'lara_django_base:view1-name'},
        {'name': 'item2',
         'path': 'lara_django_base:view2-name'},
    ])


env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)

# TODO: auth middleware should be used

__version__ = '0.2.12'


class HomeMenu(LoginRequiredMixin, TemplateView):
    # class HomeMenu(TemplateView):
    template_name = 'lara_django_base/apps.html'

    login_url = 'accounts/login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu_items'] = [  # {'name': 'Material', 'path': 'lara_django_material:part-list'},
            #  {'name': 'Devices',
            #      'path': 'lara_django_material:device-list'},
            #  {'name': 'Part',
            #      'path': 'lara_django_material:part-list'},
            #  {'name': 'Labware',
            #   'path': 'lara_django_material:labware-list'},
            #  {'name': 'Substances',
            #   'path': 'lara_django_material:labware-list'}
        ]
        #context['sparql_server_url'] = f'http://{settings.VIRTUOSO_HOST}:{settings.VIRTUOSO_ADMIN_PORT}/sparql/'
        return context

class AppsMenu(LoginRequiredMixin, TemplateView):
    # class HomeMenu(TemplateView):
    template_name = 'lara_django_base/apps.html'

    login_url = 'accounts/login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu_items'] = [  # {'name': 'Material', 'path': 'lara_django_material:part-list'},
            #  {'name': 'Devices',
            #      'path': 'lara_django_material:device-list'},
            #  {'name': 'Part',
            #      'path': 'lara_django_material:part-list'},
            #  {'name': 'Labware',
            #   'path': 'lara_django_material:labware-list'},
            #  {'name': 'Substances',
            #   'path': 'lara_django_material:labware-list'}
        ]
        context['sparql_server_url'] = f'http://{settings.VIRTUOSO_HOST}:{settings.VIRTUOSO_ADMIN_PORT}/sparql/'
        return context



class AboutView(TemplateView):
    template_name = "lara_django_base/about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        django_env = env('DJANGO_SETTINGS_MODULE')

        django_envs = {'lara_django.settings.development': 'DEVELOPMENT',
                       'lara_django.settings.production': 'PRODUCTION',
                       'lara_django.settings.staging': 'STAGING',
                       }

        try:
            django_environment = django_envs[django_env]
        except KeyError:
            django_environment = "UNKNOWN"

        context['lara_env'] = {
            'lara_version': __version__,
            'django_version': django_version,
            'lara_description': 'The LARA-django is the comprehensive science database for the LARA suite',
            'django_environment': django_environment,
            'django_settings': django_env,
            'lara_authors': ['mark doerr (mark.doerr@uni-greifswald.de)', 'benjamin lear']
        }
        return context


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class AddressDetailView(DetailView):
    model = Address

    template_name = 'lara_django_people/address_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Address - Details"
        context['update_link'] = 'lara_django_people:address-update'
        context['menu_items'] = PeopleMenu().menu_items
        return context


class AddressesListView(SingleTableView):
    model = Address
    table_class = AddressTable

    template_name = 'lara_django_people/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Addresses - List"
        context['create_link'] = 'lara_django_people:address-create'
        context['menu_items'] = PeopleMenu().menu_items
        return context


class AddressCreateView(CreateView):
    model = Address
    template_name = 'lara_django_people/create_form.html'
    form_class = AddressCreateForm
    success_url = '/people/addresses/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Address - Creation"
        return context

    def form_valid(self, form):
        print("**** form val")
        if 'cancel' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url())
        return super().form_valid(form)


class AddressUpdateView(UpdateView):
    model = Address
    template_name = 'lara_django_people/update_form.html'
    form_class = AddressUpdateForm
    success_url = '/people/addresses/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Address - Update"
        context['delete_link'] = 'lara_django_people:address-delete'

        return context


class AddressDeleteView(DeleteView):
    model = Address
    template_name = 'lara_django_people/delete_form.html'
    success_url = '/people/addresses/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Address - Delete"
        context['delete_link'] = 'lara_django_people:address-delete'
        return context
