"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC handlers*

:details: lara_django_myproj gRPC handlers.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

# generated with django-socio-grpc generateprpcinterface lara_django_base  (LARA-version)

# import logging
from django_socio_grpc.services.app_handler_registry import AppHandlerRegistry
from lara_django_base.grpc.services import (
    DatatypeService,
    MediatypeService,
    NamespaceService,
    NamespaceByURIService,
    TagService,
    ItemstatusService,
    ExtradataService,
    PhysicalunitService,
    ExternalresourceService,
    PhysicalstatematterService,
    ScientificConstantService,
    CurrencyService,
    GeolocationService,
    CountryService,
    SubdivisiontypeService,
    CountrysubdivisionService,
    CityService,
    AddressService,
    RoomcategoryService,
    RoomService,
    LocationService,
    LARAServerService,
)


def grpc_handlers(server):
    app_registry = AppHandlerRegistry("lara_django_base", server)
    # monkey patching to import the grpc module from the API
    app_registry.get_grpc_module = lambda: "lara_django_base_grpc.v1"

    app_registry.register(DatatypeService)

    app_registry.register(MediatypeService)

    app_registry.register(NamespaceService)

    app_registry.register(NamespaceByURIService)

    app_registry.register(TagService)

    app_registry.register(ItemstatusService)

    app_registry.register(ExtradataService)

    app_registry.register(PhysicalunitService)

    app_registry.register(ExternalresourceService)

    app_registry.register(PhysicalstatematterService)

    app_registry.register(ScientificConstantService)

    app_registry.register(CurrencyService)

    app_registry.register(GeolocationService)

    app_registry.register(CountryService)

    app_registry.register(SubdivisiontypeService)

    app_registry.register(CountrysubdivisionService)

    app_registry.register(CityService)

    app_registry.register(AddressService)

    app_registry.register(RoomcategoryService)

    app_registry.register(RoomService)

    app_registry.register(LocationService)

    app_registry.register(LARAServerService)
