"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC services*

:details: lara_django_myproj gRPC services.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

## generated with django-socio-grpc generateprpcinterface lara_django_base  (LARA-version)

import asyncio
from django_socio_grpc import generics, mixins
from django_socio_grpc.decorators import grpc_action
from .serializers import (
    DatatypeProtoSerializer,
    MediatypeProtoSerializer,
    NamespaceProtoSerializer,
    TagProtoSerializer,
    ItemstatusProtoSerializer,
    ExtradataProtoSerializer,
    PhysicalunitProtoSerializer,
    ExternalresourceProtoSerializer,
    PhysicalstatematterProtoSerializer,
    ScientificConstantProtoSerializer,
    CurrencyProtoSerializer,
    GeolocationProtoSerializer,
    CountryProtoSerializer,
    SubdivisiontypeProtoSerializer,
    CountrysubdivisionProtoSerializer,
    CityProtoSerializer,
    AddressProtoSerializer,
    RoomcategoryProtoSerializer,
    RoomProtoSerializer,
    LocationProtoSerializer,
    LARAServerProtoSerializer,
)

from ..filters import (
    NamespaceFilterSet,
    TagFilterSet,
    ItemStatusFilterSet,
    ExternalResourceFilterSet,
    DataTypeFilterSet,
    PhysicalUnitFilterSet,
    ScientificConstantFilterSet,
    GeoLocationFilterSet,
    CountryFilterSet,
    CountrySubdivisionFilterSet,
    CityFilterSet,
    AddressFilterSet,
    RoomCategoryFilterSet,
    RoomFilterSet,
    LocationFilterSet,
)

from lara_django_base.models import (
    DataType,
    MediaType,
    Namespace,
    Tag,
    ItemStatus,
    ExtraData,
    PhysicalUnit,
    ExternalResource,
    PhysicalStateMatter,
    ScientificConstant,
    Currency,
    GeoLocation,
    Country,
    SubdivisionType,
    CountrySubdivision,
    City,
    Address,
    RoomCategory,
    Room,
    Location,
    LARAServer,
)


class DatatypeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = DataType.objects.all()
    serializer_class = DatatypeProtoSerializer
    filterset_class = DataTypeFilterSet


class MediatypeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = MediaType.objects.all()
    serializer_class = MediatypeProtoSerializer


class NamespaceService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Namespace.objects.all()
    filterset_class = NamespaceFilterSet
    serializer_class = NamespaceProtoSerializer


class NamespaceByURIService(generics.GenericService, mixins.AsyncListModelMixin, mixins.AsyncRetrieveModelMixin):
    serializer_class = NamespaceProtoSerializer

    queryset = Namespace.objects.all().order_by("URI")

    lookup_field = "URI"


class TagService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Tag.objects.all()
    serializer_class = TagProtoSerializer
    filterset_class = TagFilterSet


class ItemstatusService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = ItemStatus.objects.all()
    serializer_class = ItemstatusProtoSerializer
    filterset_class = ItemStatusFilterSet


class ExtradataService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = ExtraData.objects.all()
    serializer_class = ExtradataProtoSerializer


class PhysicalunitService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = PhysicalUnit.objects.all()
    serializer_class = PhysicalunitProtoSerializer
    filterset_class = PhysicalUnitFilterSet


class ExternalresourceService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = ExternalResource.objects.all()
    serializer_class = ExternalresourceProtoSerializer
    filterset_class = ExternalResourceFilterSet


class PhysicalstatematterService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = PhysicalStateMatter.objects.all()
    serializer_class = PhysicalstatematterProtoSerializer
    #filterset_class = PhysicalStateMatterFilterSet


class ScientificConstantService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = ScientificConstant.objects.all()
    serializer_class = ScientificConstantProtoSerializer
    filterset_class = ScientificConstantFilterSet


class CurrencyService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Currency.objects.all()
    serializer_class = CurrencyProtoSerializer


class GeolocationService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = GeoLocation.objects.all()
    serializer_class = GeolocationProtoSerializer
    filterset_class = GeoLocationFilterSet


class CountryService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Country.objects.all()
    serializer_class = CountryProtoSerializer
    filterset_class = CountryFilterSet


class SubdivisiontypeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = SubdivisionType.objects.all()
    serializer_class = SubdivisiontypeProtoSerializer
    # filterset_class = SubdivisionTypeFilterSet


class CountrysubdivisionService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = CountrySubdivision.objects.all()
    serializer_class = CountrysubdivisionProtoSerializer


class CityService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = City.objects.all()
    serializer_class = CityProtoSerializer
    filterset_class = CityFilterSet


class AddressService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Address.objects.all()
    serializer_class = AddressProtoSerializer
    filterset_class = AddressFilterSet


class RoomcategoryService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = RoomCategory.objects.all()
    serializer_class = RoomcategoryProtoSerializer
    filterset_class = RoomCategoryFilterSet


class RoomService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Room.objects.all()
    serializer_class = RoomProtoSerializer
    filterset_class = RoomFilterSet


class LocationService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Location.objects.all()
    serializer_class = LocationProtoSerializer
    filterset_class = LocationFilterSet


class LARAServerService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = LARAServer.objects.all()
    serializer_class = LARAServerProtoSerializer
    filterset_class = LocationFilterSet
