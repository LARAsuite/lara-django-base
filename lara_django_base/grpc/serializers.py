"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC serializer*

:details: lara_django_myproj gRPC serializer.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

## generated with django-socio-grpc generateprpcinterface lara_django_base  (LARA-version)

import logging

from rest_framework.serializers import UUIDField, PrimaryKeyRelatedField
from django_socio_grpc import proto_serializers
import lara_django_base_grpc.v1.lara_django_base_pb2 as lara_django_base_pb2

from lara_django_base.models import (
    DataType,
    MediaType,
    Namespace,
    Tag,
    ItemStatus,
    ExtraData,
    PhysicalUnit,
    ExternalResource,
    PhysicalStateMatter,
    ScientificConstant,
    Currency,
    GeoLocation,
    Country,
    SubdivisionType,
    CountrySubdivision,
    City,
    Address,
    RoomCategory,
    Room,
    Location,
    LARAServer,
)


class DatatypeProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = DataType
        proto_class = lara_django_base_pb2.DatatypeResponse

        proto_class_list = lara_django_base_pb2.DatatypeListResponse

        fields = "__all__"  # [data_type', description']


class MediatypeProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = MediaType
        proto_class = lara_django_base_pb2.MediatypeResponse

        proto_class_list = lara_django_base_pb2.MediatypeListResponse

        fields = "__all__"  # [name', media_type', def_filename_extension_regex', description']


class NamespaceProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Namespace
        proto_class = lara_django_base_pb2.NamespaceResponse

        proto_class_list = lara_django_base_pb2.NamespaceListResponse

        fields = "__all__"  


class TagProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Tag
        proto_class = lara_django_base_pb2.TagResponse

        proto_class_list = lara_django_base_pb2.TagListResponse

        fields = "__all__"  # [tag']


class ItemstatusProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = ItemStatus
        proto_class = lara_django_base_pb2.ItemstatusResponse

        proto_class_list = lara_django_base_pb2.ItemstatusListResponse

        fields = "__all__"  # [status', description']


class ExtradataProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = ExtraData
        proto_class = lara_django_base_pb2.ExtradataResponse

        proto_class_list = lara_django_base_pb2.ExtradataListResponse

        fields = "__all__"  # [data_type', namespace', URI', text', XML', JSON', bin', media_type', IRI', URL', description', file', image']


class PhysicalunitProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = PhysicalUnit
        proto_class = lara_django_base_pb2.PhysicalunitResponse

        proto_class_list = lara_django_base_pb2.PhysicalunitListResponse

        fields = "__all__"  # [name', dimension', description']


class ExternalresourceProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = ExternalResource
        proto_class = lara_django_base_pb2.ExternalresourceResponse

        proto_class_list = lara_django_base_pb2.ExternalresourceListResponse

        fields = "__all__"  # [name', media_type', URI', text', XML', JSON', bin', IRI', URL', description', file', image']

class PhysicalstatematterProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = PhysicalStateMatter
        proto_class = lara_django_base_pb2.PhysicalstatematterResponse

        proto_class_list = lara_django_base_pb2.PhysicalstatematterListResponse

        fields = "__all__"  # [name', description']


class ScientificConstantProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = ScientificConstant
        proto_class = lara_django_base_pb2.ScientificConstantResponse

        proto_class_list = lara_django_base_pb2.ScientificConstantListResponse

        fields = "__all__"  # [name', value', unit', JSON', IRI', URL_source', URL_definition', description']


class CurrencyProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Currency
        proto_class = lara_django_base_pb2.CurrencyResponse

        proto_class_list = lara_django_base_pb2.CurrencyListResponse

        fields = "__all__"  # [name', IRI', alpha3code', numeric_code', symbol', exchange_rate_EUR']


class GeolocationProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = GeoLocation
        proto_class = lara_django_base_pb2.GeolocationResponse

        proto_class_list = lara_django_base_pb2.GeolocationListResponse

        fields = "__all__"  # [name', coordinates_DD_lat', coordinates_DD_long', elevation', openstreetmap', googlemap']


class CountryProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Country
        proto_class = lara_django_base_pb2.CountryResponse

        proto_class_list = lara_django_base_pb2.CountryListResponse

        fields = (
            "__all__"  # [name', name_local', alpha2code', alpha3code', numeric_code', phone_code', icon', currency']
        )


class SubdivisiontypeProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = SubdivisionType
        proto_class = lara_django_base_pb2.SubdivisiontypeResponse

        proto_class_list = lara_django_base_pb2.SubdivisiontypeListResponse

        fields = "__all__"  # [name', description']


class CountrysubdivisionProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = CountrySubdivision
        proto_class = lara_django_base_pb2.CountrysubdivisionResponse

        proto_class_list = lara_django_base_pb2.CountrysubdivisionListResponse

        fields = "__all__"  # [name', name_local', code', alpha2code', subdivision_type', country', timezone']


class CityProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = City
        proto_class = lara_django_base_pb2.CityResponse

        proto_class_list = lara_django_base_pb2.CityListResponse

        fields = "__all__"  # [name', name_local', country', subdivision', timezone']


class AddressProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Address
        proto_class = lara_django_base_pb2.AddressResponse

        proto_class_list = lara_django_base_pb2.AddressListResponse

        fields = "__all__"  # [building', street', house_number', supplement', city', subdivision', country', postal_code', geolocation']


class RoomcategoryProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = RoomCategory
        proto_class = lara_django_base_pb2.RoomcategoryResponse

        proto_class_list = lara_django_base_pb2.RoomcategoryListResponse

        fields = "__all__"  # [category', description']


class RoomProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Room
        proto_class = lara_django_base_pb2.RoomResponse

        proto_class_list = lara_django_base_pb2.RoomListResponse

        fields = "__all__"  # [name', category', number', floor', phone_number', address', room_map', max_people', area', volume']


class LocationProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Location
        proto_class = lara_django_base_pb2.LocationResponse

        proto_class_list = lara_django_base_pb2.LocationListResponse

        fields = "__all__"  # [name', barcode1D', barcode2D', geolocation', address', building', floor', room', cabinet', shelf', drawer', box', position', URI', JSON', IRI', URL', description']


class LARAServerProtoSerializer(proto_serializers.ModelProtoSerializer):
    tags = PrimaryKeyRelatedField(
        queryset=Tag.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True, many=True
    )

    class Meta:
        model = LARAServer
        proto_class = lara_django_base_pb2.LARAServerResponse

        proto_class_list = lara_django_base_pb2.LARAServerListResponse

        fields = "__all__"  # [name', description', host', port', path', version', protocol']
