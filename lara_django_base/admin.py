"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base admin *

:details: lara_django_base admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_base >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (
    DataType,
    MediaType,
    Namespace,
    Tag,
    ItemStatus,
    ExtraData,
    ExternalResource,
    PhysicalUnit,
    PhysicalStateMatter,
    ScientificConstant,
    Currency,
    GeoLocation,
    Country,
    SubdivisionType,
    CountrySubdivision,
    City,
    Address,
    RoomCategory,
    Room,
    Location,
    LARAServer,
)


@admin.register(DataType)
class DataTypeAdmin(admin.ModelAdmin):
    list_display = ("data_type_id", "data_type", "description")


@admin.register(MediaType)
class MediaTypeAdmin(admin.ModelAdmin):
    list_display = (
        "media_type_id",
        "name",
        "media_type",
        "def_filename_extension_regex",
        "description",
    )
    search_fields = ("name",)


@admin.register(Namespace)
class NamespaceAdmin(admin.ModelAdmin):
    list_display = (
        "URI",
        "description",
        "namespace_id",
    )
    search_fields = ("URI",)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("tag", "IRI", "tag_id")


@admin.register(ItemStatus)
class ItemStatusAdmin(admin.ModelAdmin):
    list_display = ("status_id", "status", "description")


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        "name_full",
        "name",
        "namespace",
        "data_type",
        "IRI",
        "data_JSON",
        "media_type",
        "description",
        "extra_data_id",
        "file",
    )
    list_filter = ("data_type", "namespace", "media_type")


@admin.register(ExternalResource)
class ExternalResourceAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "URL",
        "IRI",
        "external_resource_id",
    )
    list_filter = ("name", "tags", "URL", "IRI")
    # raw_id_fields = ()


@admin.register(PhysicalStateMatter)
class PhysicalStateMatterAdmin(admin.ModelAdmin):
    list_display = ("physical_state_matter_id", "name", "description")
    search_fields = ("name",)


@admin.register(PhysicalUnit)
class PhysicalUnitAdmin(admin.ModelAdmin):
    list_display = ("unit_id", "name", "dimension", "description")
    search_fields = ("name",)


@admin.register(ScientificConstant)
class ScientificConstantAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "value",
        "unit",
        "data_JSON",
        "IRI",
        "URL_source",
        "URL_definition",
        "description",
        "constants_id",
    )
    list_filter = ("unit",)
    search_fields = ("name",)


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "IRI",
        "alpha3code",
        "numeric_code",
        "symbol",
        "exchange_rate_EUR",
        "currency_id",
    )
    search_fields = ("name",)


@admin.register(GeoLocation)
class GeoLocationAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "coordinates_DD_lat",
        "coordinates_DD_long",
        "elevation",
        "openstreetmap",
        "googlemap",
        "geoinfo_id",
    )
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_local",
        "alpha2code",
        "alpha3code",
        "numeric_code",
        "phone_code",
        "icon",
        "currency",
        "country_id",
    )
    raw_id_fields = ("currency",)
    search_fields = ("name",)


@admin.register(SubdivisionType)
class SubdivisionTypeAdmin(admin.ModelAdmin):
    list_display = ("subdivision_type_id", "name", "description")
    search_fields = ("name",)


@admin.register(CountrySubdivision)
class CountrySubdivisionAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_local",
        "code",
        "alpha2code",
        "subdivision_type",
        "country",
        "timezone",
        "subdivision_id",
    )
    raw_id_fields = ("subdivision_type", "country")
    search_fields = ("name",)


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_local",
        "country",
        "subdivision",
        "timezone",
        "city_id",
    )
    list_filter = ("country", "subdivision")
    search_fields = ("name",)


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = (
        "building",
        "street",
        "house_number",
        "supplement",
        "city",
        "subdivision",
        "country",
        "postal_code",
        "geolocation",
        "address_id",
    )
    list_filter = ("city", "subdivision", "country", "geolocation")
    raw_id_fields = ("data_extra",)


@admin.register(RoomCategory)
class RoomCategoryAdmin(admin.ModelAdmin):
    list_display = ("room_cat_id", "category", "description")


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "category",
        "number",
        "floor",
        "phone_number",
        "address",
        "room_map",
        "max_people",
        "area",
        "volume",
        "room_id",
    )
    list_filter = ("category", "address")
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "barcode1D",
        "barcode2D",
        "geolocation",
        "address",
        "building",
        "floor",
        "room",
        "cabinet",
        "shelf",
        "drawer",
        "box",
        "position",
        "data_JSON",
        "IRI",
        "URL",
        "description",
        "location_id",
    )
    list_filter = ("geolocation", "address", "room")
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(LARAServer)
class LARAServerAdmin(admin.ModelAdmin):
    list_display = (
        "URL",
        "port",
        "description",
        "server_id",
    )
    list_filter = ("description", "URL")
    search_fields = ("URL", "tags")
