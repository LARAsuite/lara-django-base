"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base admin *

:details: lara_django_base admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_base >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_base
# generated with django-extensions tests_generator  lara_django_base > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import (
    DataType,
    MediaType,
    Namespace,
    Tag,
    ItemStatus,
    ExtraData,
    PhysicalUnit,
    ScientificConstant,
    Currency,
    GeoLocation,
    Country,
    SubdivisionType,
    CountrySubdivision,
    City,
    Address,
    RoomCategory,
    Room,
    Location,
)


class DataTypeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:datatype-detail', [tables.A('pk')]))

    class Meta:
        model = DataType

        fields = ("data_type", "description")


class MediaTypeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:mediatype-detail', [tables.A('pk')]))

    class Meta:
        model = MediaType

        fields = ("name", "media_type", "def_filename_extension_regex", "description")


class NamespaceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:namespace-detail', [tables.A('pk')]))

    class Meta:
        model = Namespace

        fields = ("domain", "subdomain", "URI", "description")


class TagTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:tag-detail', [tables.A('pk')]))

    class Meta:
        model = Tag

        fields = ("tag",)


class ItemStatusTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:itemstatus-detail', [tables.A('pk')]))

    class Meta:
        model = ItemStatus

        fields = ("status", "description")


class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
            "data_type",
            "namespace",
            "URI",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
        )


class PhysicalUnitTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:physicalunit-detail', [tables.A('pk')]))

    class Meta:
        model = PhysicalUnit

        fields = ("name", "dimension", "description")


class ScientificConstantTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:ScientificConstant-detail', [tables.A('pk')]))

    class Meta:
        model = ScientificConstant

        fields = (
            "name",
            "value",
            "unit",
            "IRI",
            "URL_source",
            "URL_definition",
            "description",
        )


class CurrencyTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:currency-detail', [tables.A('pk')]))

    class Meta:
        model = Currency

        fields = (
            "name",
            "IRI",
            "alpha3code",
            "numeric_code",
            "symbol",
            "exchange_rate_EUR",
        )


class GeoLocationTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:geolocation-detail', [tables.A('pk')]))

    class Meta:
        model = GeoLocation

        fields = (
            "name",
            "coordinates_DD_lat",
            "coordinates_DD_long",
            "elevation",
            "openstreetmap",
            "googlemap",
        )


class CountryTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:country-detail', [tables.A('pk')]))

    class Meta:
        model = Country

        fields = (
            "name",
            "name_local",
            "alpha2code",
            "alpha3code",
            "numeric_code",
            "phone_code",
            "icon",
            "currency",
        )


class SubdivisionTypeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:subdivisiontype-detail', [tables.A('pk')]))

    class Meta:
        model = SubdivisionType

        fields = ("name", "description")


class CountrySubdivisionTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:countrysubdivision-detail', [tables.A('pk')]))

    class Meta:
        model = CountrySubdivision

        fields = (
            "name",
            "name_local",
            "code",
            "alpha2code",
            "subdivision_type",
            "country",
            "timezone",
        )


class CityTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:city-detail', [tables.A('pk')]))

    class Meta:
        model = City

        fields = ("name", "name_local", "country", "subdivision", "timezone")


class AddressTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:address-detail', [tables.A('pk')]))

    class Meta:
        model = Address

        fields = (
            "building",
            "street",
            "house_number",
            "supplement",
            "city",
            "subdivision",
            "country",
            "postal_code",
            "geolocation",
        )


class RoomCategoryTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:roomcategory-detail', [tables.A('pk')]))

    class Meta:
        model = RoomCategory

        fields = ("category", "description")


class RoomTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:room-detail', [tables.A('pk')]))

    class Meta:
        model = Room

        fields = (
            "name",
            "category",
            "number",
            "floor",
            "phone_number",
            "address",
            "room_map",
            "max_people",
            "area",
            "volume",
        )


class LocationTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_base:location-detail', [tables.A('pk')]))

    class Meta:
        model = Location

        fields = (
            "name",
            "barcode1D",
            "barcode2D",
            "geolocation",
            "address",
            "building",
            "floor",
            "room",
            "cabinet",
            "shelf",
            "drawer",
            "box",
            "position",
            "URI",
            "IRI",
            "URL",
            "description",
        )
