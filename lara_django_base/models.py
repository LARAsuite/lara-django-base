"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base models *

:details: lara_django_base database models.
         - 
         - fixtures can be generated with
         lara-django-dev dumpdata lara_django_base.itemstatus --indent 2 --natural-primary > 2_itemstatus_fix.json

:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import logging
import datetime
from typing import Any
import uuid
import json
import hashlib
from random import randint
from urllib.parse import urlparse

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator, RegexValidator
from .lara_color_schemes import ColorBlind16


settings.FIXTURES += [
    "1_currency_fix",
    "2_datatypes_fix",
    "2_itemstatus_fix",
    "2_tags_fix",
    "3_geolocation_fix",
    # "4_location_fix",
    "4_room_category_fix",
    "1_countries_fix",
    "3_country_subdiv_type_fix",
    "4_country_subdiv_fix",
    "4_cities_fix",
    "5_namespace_fix",
    "6_iana_media_type_fix",
    "9_physical_unit_fix",
    "9_physical_state_matter_fix",
    "9_scientific_constants",
]


class URNField(models.CharField):
    """URN field that accepts URLs that adhere the scheme for URNs  only."""

    default_validators = [
        RegexValidator(
            regex=r"^urn:[a-z0-9][a-z0-9-]{1,31}:([a-z0-9()+,-.:=@;$_!*\'%/?#]|%[0-9a-f][0-9a-f])*$",
            message="Please enter a valid URN, s. https://en.wikipedia.org/wiki/Uniform_Resource_Name",
            code="invalid_URN",
        )
    ]  # [URLValidator(schemes=['urn'])]

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.max_length = 512


class MediaType(models.Model):
    """generic media Type (IANA = new MIMEsystem, see https://www.iana.org/assignments/media-types/media-types.xhtml):
    for media files, like e.g. svg, JSON, png, mp3, pdf, ...
    Format:
    type / subtype *(; parameter) application/x-animl, application/json
    """

    model_CURI = "lara:base/MediaType"

    media_type_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True, help_text="name of the IANA application")
    # TODO: check for uniqueness of csv file
    media_type = models.TextField(
        null=True,
        help_text="""name of IANA/MIME media type in the format: type "/" [tree "."] subtype ["+" suffix] *[";" parameter]""",
    )  # unique=True,
    def_filename_extension_regex = models.TextField(
        unique=True,
        null=True,
        help_text="default filename extension regular expression, e.g. ^.*\.(jpg|JPG)$ ,  ^.*\.(gif|GIF|doc|DOC|pdf|PDF)$  ",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="description of file type")

    class Meta:
        ordering = ["name"]

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/MediaType/" + self.name.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)

    def __str__(self):
        return self.media_type or ""

    def __repr__(self):
        return self.media_type or ""


class DataType(models.Model):
    """Generic DataType that can be used to classify ExtraData (s. below)
    e.g. email_home, email_lab, telephone_number_home1, company_x_customer_number,
     sequence data, absorption data ...
    """

    model_CURI = "lara:base/DataType"

    data_type_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data_type = models.TextField(unique=True, null=True, help_text="name of extra data type")
    media_type = models.ForeignKey(
        MediaType,
        related_name="%(app_label)s_%(class)s_file_types_related",
        related_query_name="%(app_label)s_%(class)s_file_types_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="IANA media type of extra data file",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="description of extra data type")

    class Meta:
        ordering = ["data_type"]

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/DataType/" + self.data_type.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.data_type or ""

    def __repr__(self):
        return self.data_type or ""


class Namespace(models.Model):
    """generic namespace model:
    Namespaces can be used to specify the scope or provenience of data, entities, etc. and helps to make them unique.
    """

    model_CURI = "lara:base/Namespace"
    namespace_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    URI = models.URLField(
        blank=True,
        unique=True,
        help_text="Universal Resource Identifier - A unique namespace for identifying data, https://de.unigreifswald/biochem/akb",
    )
    description = models.TextField(blank=True, null=True, help_text="description of this Namespace")

    class Meta:
        ordering = ["URI"]

    def parse_URI(self):
        """parse URI into parts
        to retrieve the parts of the URI, use the following attributes (example: https://de.unigreifswald/biochem/akb"):
           result = parse_URI()
             result.scheme -> "https"
             result.netloc -> "de.unigreifswald"
             result.path -> "/biochem/akb"
        """
        return urlparse(self.URI)

    def __str__(self):
        return self.URI or ""

    def __repr__(self):
        return self.URI or ""


class Tag(models.Model):
    """generic tags and keywords model:
    Tags and keywords can be used to group related information or make objects findable.
    """

    model_CURI = "lara:base/Tag"
    tag_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    tag = models.TextField(
        help_text="a tag, can be used for tagging items, like 'reviewed', 'interesting', but also for keywords, like 'python', 'evolution'"
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/Tag/" + self.tag.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)

    class Meta:
        ordering = ["tag"]

    def __str__(self):
        return self.tag or ""


class ItemStatus(models.Model):
    """generic item status/state, can be used for indicating the status of LARA items,
    like a containers or processes: planned, started, evaluating
    please do not use tags for this - separating tag and status makes processing clearer (hopefully :)
    """

    model_CURI = "lara:base/ItemStatus"
    status_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    status = models.TextField(help_text="item status, like planned, started, evaluating, .. finished")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="A short description of the item status")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/ItemStatus/" + self.status.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.status or ""

    class Meta:
        verbose_name_plural = "ItemStatus"


class BlockchainHashesAbstr(models.Model):
    """This class can be used to store hashes of data,
    e.g. for blockchain applications
    """

    blockchain_hashes_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    hash_SHA256 = models.CharField(
        max_length=256,
        unique=True,
        blank=True,
        null=True,
        default=None,
        help_text="SHA256 hash of all data (JSON and XML)",
    )
    hash_SHA512 = models.CharField(
        max_length=512,
        unique=True,
        blank=True,
        null=True,
        default=None,
        help_text="SHA512 hash of all data (JSON and XML)",
    )
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when the blockchain hash was last modified"
    )

    class Meta:
        abstract = True
        verbose_name_plural = "BlockchainHashes"


class ExtraDataAbstr(models.Model):
    """This class can be used to extend data, by extra information,
    e.g. more telephone numbers, customer numbers, ..."""

    extra_data_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data_type = models.ForeignKey(
        DataType,
        related_name="%(app_label)s_%(class)s_data_type_related",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    namespace = models.ForeignKey(
        Namespace,
        related_name="%(app_label)s_%(class)s_namespaces_related",
        related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="namespace of data",
    )
    title = models.TextField(blank=True, null=True, help_text="Human readable title of extra data")
    name = models.TextField(blank=True, null=True, help_text="name of extra data")
    name_full = models.TextField(unique=True, blank=True, null=True, help_text="full name of extra data")

    version = models.TextField(default="0.0.1", blank=True, null=True, help_text="version of extra data")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="data UUID")
    hash_SHA256 = models.CharField(
        max_length=256,
        unique=True,
        blank=True,
        null=True,
        default=None,
        help_text="SHA256 hash of all data (JSON and XML)",
    )
    data_JSON = models.JSONField(blank=True, null=True, help_text="generic JSON field")
    media_type = models.ForeignKey(
        MediaType,
        related_name="%(app_label)s_%(class)s_file_types_related",
        related_query_name="%(app_label)s_%(class)s_file_types_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="IANA media type of extra data file",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    URL = models.URLField(
        blank=True, help_text="Universal Resource Locator - this can be used to link the data to external locations"
    )
    datetime_created = models.DateTimeField(default=timezone.now, help_text="date and time when data was created")
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )
    description = models.TextField(blank=True, null=True, help_text="description of extra data")

    def __str__(self):
        return f"{self.name_full}" or ""

    def __repr__(self):
        return f"{self.name_full}" or ""

    class Meta:
        abstract = True
        verbose_name_plural = "ExtraData"


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
    e.g. more telephone numbers, customer numbers, ..."""

    model_CURI = "lara:base/ExtraData"
    extra_data_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(upload_to="base", blank=True, null=True, help_text="rel. path/filename")
    image = models.ImageField(
        upload_to="base/images/",
        blank=True,
        default="image.svg",
        help_text="location room map rel. path/filename to image",
    )

    # def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if self.hash_SHA256 is None:
            if self.data_JSON is not None:
                to_hash = json.dumps(self.data_JSON)  # +  str(self.UUID) +
                self.hash_SHA256 = hashlib.sha256(to_hash.encode("utf-8")).hexdigest()
            else:
                self.hash_SHA256 = hashlib.sha256(str(self.UUID).encode("utf-8")).hexdigest()

        if self.name is None or self.name == "":
            if self.title is None or self.title == "":
                self.name = "_".join(
                    (
                        timezone.now().strftime("%Y%m%d_%H%M%S"),
                        "data",
                        self.hash_SHA256[:8],
                    )
                )
            else:
                self.name = self.title.replace(" ", "_").lower().strip()

        else:
            self.name = self.name.replace(" ", "_").lower().strip()
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None and self.version is not None:
                self.name_full = f"{self.namespace.URI}/" + "_".join((self.name.replace(" ", "_"), self.version))
            else:
                self.name_full = "_".join((self.name.replace(" ", "_"), self.version))

        if not self.IRI:
            netloc = self.namespace.parse_URI().netloc
            path = self.namespace.parse_URI().path

            self.IRI = f"{settings.LARA_PREFIXES['lara']}base/ExtraData/{netloc}{path}/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)


class ExternalResource(models.Model):
    """generic external resource class:
    External resources can be used to link to external resources, like e.g. literature, pdf, a website, a file, a database, ...
    """

    model_CURI = "lara:base/ExternalResource"
    external_resource_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField(blank=True, null=True, help_text="Human readable title of external resource")
    name = models.TextField(unique=True, blank=True, null=True, help_text="name of the external resource")
    resource_type = models.ForeignKey(
        DataType,
        related_name="%(app_label)s_%(class)s_resource_type_related",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="type of external resource, like pdf, website, handle, DOI, ...",
    )
    URL = models.URLField(
        blank=True,
        help_text="Universal Resource Locator - URL - this can be used to link the data to external locations",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="Universal Resource Name - URI - this can be used to uniquely identify the external resource",
    )
    prefix = models.TextField(
        null=True,
        blank=True,
        help_text="prefix for compact URIs (CURI), e.g. 'skos' for 'http://www.w3.org/2004/02/skos/core#'",
    )
    tags = models.ManyToManyField(
        Tag,
        related_name="%(app_label)s_%(class)s_tags_related",
        related_query_name="%(app_label)s_%(class)s_tags_related_query",
        blank=True,
        help_text="tags for external resource",
    )
    description = models.TextField(blank=True, null=True, help_text="description of the external resource")
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if self.title is not None or "":
            if self.name is None or self.name == "":
                self.name = self.title.replace(" ", "_").lower().strip()
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/ExternalResource/" + self.name.replace(" ", "_").lower().strip()
            )
        super().save(*args, **kwargs)


class PhysicalUnit(models.Model):
    """physical units, like, meter, kilogram, ..., preferably SI units"""

    model_CURI = "lara:base/PhysicalUnit"
    unit_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(help_text="physical unit, like meter, kilogram, ...")
    symbol = models.TextField(blank=True, null=True, help_text="symbol of the physical unit, like m, kg, ...")
    dimension = models.TextField(blank=True, null=True, help_text="physical dimension, like length, mass, ...")
    transformation_SI = models.JSONField(
        blank=True,
        null=True,
        help_text="transformation formula to SI unit, in machine readable form (e.g. python code), like: x * 1000.0",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="A short description of the physical unit")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/PhysicalUnit/" + self.name.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ""


# TODO: should be removed, as it is over modelled
class PhysicalStateMatter(models.Model):
    """Physical state of matter, like solid, liquid, gas, plasma, ..."""

    model_CURI = "lara:base/PhysicalStateMatter"
    physical_state_matter_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(help_text="physical state of matter, like solid, liquid, gas, plasma, ...")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(
        blank=True, null=True, help_text="A short description of the physical state of matter"
    )

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""


class ScientificConstant(models.Model):
    """_Mathematical and Physical constants_

    :param models: _description_
    :type models: _type_
    :raises ValidationError: _description_
    :return: _description_
    :rtype: _type_
    """

    model_CURI = "lara:base/ScientificConstant"
    constants_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True, help_text="generic text field")
    symbol = models.TextField(blank=True, null=True, help_text="symbol of the constant")
    value = models.FloatField(blank=True, null=True, help_text="Float value of the ")
    uncertainty = models.FloatField(blank=True, null=True, help_text="uncertainty of the value +/-")
    unit = models.ForeignKey(
        PhysicalUnit,
        related_name="%(app_label)s_%(class)s_units_related",
        related_query_name="%(app_label)s_%(class)s_units_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="physical unit",
    )
    data_JSON = models.JSONField(blank=True, null=True, help_text="complex content")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    URL_source = models.URLField(blank=True, help_text="Universal Resource Locator - URL - source, e.g. in literature")
    URL_definition = models.URLField(blank=True, help_text="Universal Resource Locator - URL - definition")
    description = models.TextField(blank=True, null=True, help_text="description of extra data")
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"]
                + "base/ScientificConstant/"
                + self.name.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name}" or ""

    def __repr__(self):
        return f"{self.name} ({self.description})" or ""


class Currency(models.Model):
    """generic currency class
    s. also Money class from django-shop
    """

    model_CURI = "lara:base/Currency"
    currency_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="currency name")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    alpha3code = models.TextField(blank=True, help_text="acronym, e.g. EUR, USD, ..")
    numeric_code = models.IntegerField(
        blank=True, null=True, help_text="International 3 number code, like 978 (for EUR) "
    )
    symbol = models.TextField(blank=True, help_text="currency symbol, e.g. €, $, ...")
    exchange_rate_EUR = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="currency exchange rate -> Euro "
    )
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )

    class Meta:
        verbose_name_plural = "Currencies"

    def __str__(self):
        return self.name or ""

    def in_Euro(self, amount: float = 1.0):
        return amount * self.exchange_rate_EUR

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/Currency/" + self.name.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)


class GeoLocation(models.Model):
    """generic geo location information class to store geo location data,
    like Decimal Degrees of the location of, e.g. a building, a sample, etc. ...

     .. todo:: accept only one input format, add converters to common formats
    """

    model_CURI = "lara:base/GeoLocation"
    geoinfo_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(unique=True, help_text="Unique location name")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    coordinates_DD_lat = models.DecimalField(
        max_digits=20, decimal_places=9, default=0.0, help_text="in Decimal degrees (DD) - latitude, e.g., 54.0915068"
    )
    coordinates_DD_long = models.DecimalField(
        max_digits=20, decimal_places=9, default=0.0, help_text="in Decimal degrees (DD) - longitude, e.g., 13.4029247 "
    )
    elevation = models.IntegerField(default=0, help_text="elevation in m above sea level")
    # this could be autogenerated upon saved...
    openstreetmap = models.URLField(blank=True, help_text="OpenStreetMap (www.openstreetmap.org) link to address")
    # this could be autogenerated upon saved...
    googlemap = models.URLField(blank=True, help_text="google maps link to address")
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_data_extra_related_query",
        blank=True,
        help_text="this can be used to extend the geo location: like maps to location etc.",
    )
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )

    def __str__(self):
        return self.name or ""  # self.openstreetmap

    # coordinate converters
    def to_DMS(self):
        """converting DD to Degrees, minutes, and seconds (DMS)"""
        # return DD_to_DMS( self.coordinates_DD_lat, self.coordinates_DD_long)
        # coordinates_DMS =  models.TextField(blank=True, default="""0deg0'0.0"N 0deg0'0.0"E""", help_text="""in Degrees, minutes, and seconds (DMS): 54deg5'29.434"N 13deg24'10.527"E""") # might need a syntax checker

    def to_DMM(self):
        """converting DD to Degrees and decimal minutes (DMM)"""
        # return DD_to_DMM(self.coordinates_DD_lat, self.coordinates_DD_long)
        # coordinates_DMM =  models.TextField(blank=True, default="0 0.0, 0 0.0", help_text="in Degrees and decimal minutes (DMM): 41 24.2028, 2 10.4418")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/GeoLocation/" + self.name.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)


class Country(models.Model):
    """generic country representation class"""

    model_CURI = "lara:base/Country"
    country_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="international name of the country")
    name_local = models.TextField(blank=True, help_text="local name of the country, e.g. Deutschland")
    alpha2code = models.CharField(
        max_length=2, blank=True, help_text="International 2-letter code, like DE, SE, DK ..."
    )
    alpha3code = models.CharField(
        max_length=3, blank=True, help_text="International 3-letter code, like DEU, USA, HEL ..."
    )
    numeric_code = models.IntegerField(blank=True, null=True, help_text="International 3 number code, like ")
    phone_code = models.IntegerField(
        blank=True, default="00", help_text="International telephone code, like 49, 45, ..."
    )
    icon = models.TextField(blank=True, null=True, help_text="XML/SVG icon / symbol / flag of the country")
    currency = models.ForeignKey(
        Currency,
        related_name="%(app_label)s_%(class)s_currencies_related",
        related_query_name="%(app_label)s_%(class)s_currencies_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="currency",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="description of the country")

    def __str__(self):
        return self.name_local or ""

    class Meta:
        verbose_name_plural = "Countries"

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/Country/" + self.name.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)


class SubdivisionType(models.Model):
    """Country Subdivision Type: e.g. State, Province, Emirate, ..."""

    model_CURI = "lara:base/SubdivisionType"
    subdivision_type_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )  # models.AutoField( primary_key=True)  #
    name = models.TextField(unique=True, null=True, help_text="subdivision type, e.g. State, Province, Emirate, ... ")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="description of subdivision type")

    def __str__(self):
        return self.name or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/SubdivisionType/" + self.name.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)


class CountrySubdivision(models.Model):
    """Generic country subdivision  to fit federal states, provinces, counties etc. into the same concept
    Not limited to US federal states - also Germany and many other countries have them, s. pypi iso3166 and pycountry"""

    model_CURI = "lara:base/CountrySubdivision"
    subdivision_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        blank=True, help_text="international name of the subdivision/federal state, Hessia, Thuringia, Baveria, ..."
    )
    name_local = models.TextField(blank=True, help_text="local name of the state, e.g. Hessen, Thüringen")
    code = models.CharField(max_length=16, blank=True, help_text="multi-letter code, like US-MA, DE-TH ,  ...")
    alpha2code = models.CharField(max_length=3, blank=True, help_text="2-letter code, like MA, BY, DK ...")
    subdivision_type = models.ForeignKey(
        SubdivisionType,
        related_name="%(app_label)s_%(class)s_subdivision_types_related",
        related_query_name="%(app_label)s_%(class)s_subdivision_types_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    country = models.ForeignKey(
        Country,
        related_name="%(app_label)s_%(class)s_countries_related",
        related_query_name="%(app_label)s_%(class)s_countries_related_query",
        on_delete=models.CASCADE,
        null=True,
        help_text="country",
    )
    timezone = models.IntegerField(default=0, help_text="time zone offest from UTC, e.g. 5, -3 ")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="description of the country subdivision")

    def __str__(self):
        if self.name_local != "":
            return f"{self.name} ({self.name_local})" or ""
        else:
            return self.name or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"]
                + "base/CountrySubdivision/"
                + self.name.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)


class City(models.Model):
    """Not limited to US federal states - also Germany has them"""

    model_CURI = "lara:base/City"
    city_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="international name of the city, e.g. Munich")
    name_local = models.TextField(blank=True, help_text="local name of the city, e.g. München")
    country = models.ForeignKey(
        Country,
        related_name="%(app_label)s_%(class)s_contries_related",
        related_query_name="%(app_label)s_%(class)s_countries_related_query",
        on_delete=models.CASCADE,
        null=True,
        help_text="country",
    )
    subdivision = models.ForeignKey(
        CountrySubdivision,
        related_name="%(app_label)s_%(class)s_subdivisions_related",
        related_query_name="%(app_label)s_%(class)s_subdivisions_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="country subdivision where the city is loacted",
    )
    timezone = models.IntegerField(default=0, help_text="time zone offest from UTC, e.g. 5, -3 ")
    # ZIP codes (in extra table)
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="description of the city")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/City/" + self.name.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name} ({self.name}/{self.country})" or ""

    def __repr__(self):
        return f"{self.name} ({self.name}/{self.country})" or ""

    class Meta:
        verbose_name_plural = "Cities"


class Address(models.Model):
    """generic Address class:
    this class can be used to store all kind of Addresses"""

    model_CURI = "lara:base/Address"
    address_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    building = models.TextField(blank=True, help_text="in case the institution is within a bigger complex/campus")
    street = models.TextField(blank=True, help_text="street, excluding house number")
    house_number = models.TextField(blank=True, help_text="house number")
    supplement = models.TextField(
        blank=True, help_text="address supplement info to make it unique, like c/o, room/floor"
    )
    # co  = models.ForeignKey('Entity', related_name="addresses", on_delete=models.CASCADE, blank=True, null=True, help_text="care of - c/o field")
    city = models.ForeignKey(
        City,
        related_name="%(app_label)s_%(class)s_cities_related",
        related_query_name="%(app_label)s_%(class)s_cities_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="city",
    )
    subdivision = models.ForeignKey(
        CountrySubdivision,
        related_name="%(app_label)s_%(class)s_subdivisions_related",
        related_query_name="%(app_label)s_%(class)s_subdivisions_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="e.g. federal state, like Thuringia or Alaska",
    )
    country = models.ForeignKey(
        Country,
        related_name="%(app_label)s_%(class)s_countries_related",
        related_query_name="%(app_label)s_%(class)s_countries_related_query",
        on_delete=models.CASCADE,
        null=True,
        help_text="country",
    )
    postal_code = models.TextField(blank=True, help_text="postal code / ZIP")
    geolocation = models.ForeignKey(GeoLocation, on_delete=models.CASCADE, null=True, help_text="Geographical location")
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_data_extra_related_query",
        blank=True,
        help_text="extra information to addresses",
    )
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )
    # IRI = models.URLField(
    #     blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    description = models.TextField(blank=True, null=True, help_text="description of the address")

    def __str__(self):
        return f"{self.street}, {self.postal_code}, {self.city}" or ""

    def __repr__(self):
        return (
            f"{self.building}, {self.street}, {self. house_number}, {self.country}, {self.postal_code}, {self.city}"
            or ""
        )

    class Meta:
        verbose_name_plural = "Addresses"


class RoomCategory(models.Model):
    """room category: office, lab, lab_S1, lab_S2, ... seminar room"""

    model_CURI = "lara:base/RoomCategory"
    room_cat_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    category = models.TextField(unique=True, null=True, help_text="room category")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="room category description")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = (
                settings.LARA_PREFIXES["lara"] + "base/RoomCategory/" + self.name.replace(" ", "_").lower().strip()
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.category or ""


class Room(models.Model):
    """Rooms and laboratories"""

    model_CURI = "lara:base/Room"
    room_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="name of the room - some rooms have names")
    category = models.ForeignKey(
        RoomCategory,
        related_name="%(app_label)s_%(class)s_categories_related",
        related_query_name="%(app_label)s_%(class)s_categories_related_query",
        on_delete=models.CASCADE,
        null=True,
        help_text="room category",
    )
    number = models.TextField(blank=True, help_text="room number")
    floor = models.TextField(blank=True, help_text="Floor")
    phone_number = models.TextField(
        blank=True, help_text="room phone number, if possible add the +Country code notation, e.g., +44 3834 420 4411"
    )
    address = models.ForeignKey(
        Address,
        related_name="%(app_label)s_%(class)s_addresses_related",
        related_query_name="%(app_label)s_%(class)s_addresses_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="reference to an address",
    )  # check, if it can be 0 !
    room_map = models.ImageField(
        upload_to="people/rooms/",
        blank=True,
        default="room.svg",
        help_text="location room map rel. path/filename to image",
    )
    # further info about the room
    max_people = models.IntegerField(default=1, help_text="max number of people for this room")
    area = models.DecimalField(
        max_digits=9, decimal_places=2, blank=True, null=True, help_text="room area in square meters"
    )
    volume = models.DecimalField(
        max_digits=9, decimal_places=2, blank=True, null=True, help_text="room volume in cubic meters"
    )
    # feature, e.g., sockets, lights, gas, furniture, infrastructure, ...
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="extra_data_rooms",
        blank=True,
        help_text="extra information to this room, like sockets, lights, gas, furniture, infrastructure,... ",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, null=True, help_text="room description")

    def __str__(self):
        return "{} ({})".format(self.number, self.floor) or ""

    def __repr__(self):
        return "{} ({})".format(self.number, self.floor) or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/Room/" + self.name.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)


class Location(models.Model):
    """Pysical location of objects like device, substances, organisms, containers etc.,
    at concrete place, e.g., in shelf or drawer in a lab or room.
    .. todo: a foreign key to a device for modelling a location in a device could be considered,
             but generates an additional dependency.....
    """

    model_CURI = "lara:base/Location"
    location_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(unique=True, help_text="Unique location name")
    barcode1D = models.TextField(blank=True, null=True, help_text="1D barcode of location")
    barcode2D = models.BinaryField(blank=True, null=True, help_text="2D barcode of location - binary")
    geolocation = models.ForeignKey(
        GeoLocation,
        related_name="%(app_label)s_%(class)s_geolocations_related",
        related_query_name="%(app_label)s_%(class)s_geolocations_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="geo location",
    )
    address = models.ForeignKey(
        Address,
        related_name="%(app_label)s_%(class)s_addresses_related",
        related_query_name="%(app_label)s_%(class)s_addresses_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="reference to an address",
    )
    building = models.TextField(blank=True, help_text="building identifier")
    floor = models.TextField(blank=True, help_text="floor identifier")
    room = models.ForeignKey(
        Room,
        related_name="%(app_label)s_%(class)s_rooms_related",
        related_query_name="%(app_label)s_%(class)s_rooms_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="reference to a room",
    )
    cabinet = models.TextField(blank=True, help_text="cabinet identifier")
    shelf = models.TextField(blank=True, help_text="shelf identifier")
    drawer = models.TextField(blank=True, help_text="drawer identifier")
    box = models.TextField(blank=True, help_text="box identifier")
    position = models.JSONField(
        blank=True, null=True, help_text="position in room, cabinet, shelf, drawer or box - coordnates possible"
    )
    data_JSON = models.JSONField(
        blank=True,
        null=True,
        help_text="JSON field for advanced location specification - can be used to store, e.g. locations in a device",
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    URL = models.URLField(
        blank=True,
        help_text="Universal Resource Locator - this can be used to link this location to an external location",
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_data_extra_related_query",
        blank=True,
        help_text="like maps to location etc.",
    )
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )
    description = models.TextField(blank=True, help_text="location description")

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return " ".join((self.name, self.description)) or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for the IRI
        """
        if not self.IRI:
            self.IRI = settings.LARA_PREFIXES["lara"] + "base/Location/" + self.name.replace(" ", "_").lower().strip()

        super().save(*args, **kwargs)


class LARAServer(models.Model):
    """LARA Server description, e.g., for synchronisation between two LARA instances"""

    model_CURI = "lara:base/LARAServer"
    server_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    URL = models.URLField(blank=True, help_text="Universal Resource Locator - URL of the LARA server")
    port = models.IntegerField(blank=True, null=True, help_text="port of the LARA server")
    coonection_JSON = models.JSONField(
        blank=True,
        null=True,
        help_text="JSON field for more advanced connection information, e.g. advanced routing, ports, ..., could be even encrypted.",
    )
    auth_JSON = models.JSONField(
        blank=True,
        null=True,
        help_text="JSON field for authentication information, e.g. username, password, token, ..., could be even encrypted",
    )
    cert_pub = models.TextField(blank=True, help_text="server public certificate")

    tags = models.ManyToManyField(
        Tag,
        related_name="%(app_label)s_%(class)s_tags_related",
        related_query_name="%(app_label)s_%(class)s_tags_related_query",
        blank=True,
        help_text="tags LARA server",
    )
    description = models.TextField(blank=True, help_text="server description")

    def __str__(self):
        return self.URL or ""


class SyncStatusAbstr(models.Model):
    """An abstract class to store sync status information, e.g. for synchronisation between two LARA instances"""

    sync_status_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    status = models.ForeignKey(
        ItemStatus,
        related_name="%(app_label)s_%(class)s_statuses_related",
        related_query_name="%(app_label)s_%(class)s_statuses_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="scheduled, in-progress, interrupted, completed",
    )
    server = models.ForeignKey(
        LARAServer,
        related_name="%(app_label)s_%(class)s_servers_related",
        related_query_name="%(app_label)s_%(class)s_servers_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    datetime_last_synced = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )
    # sync_conflicts = models.FileField(help_text="File containing detailed information about conflicts during synchronisation")

    class Meta:
        abstract = True
        verbose_name_plural = "SyncStatus"


class SynchronisationAbstr(models.Model):
    """Abstract Synchronisation settings class for (recursive) LARA projects synchronisation with other LARA systems

    :param models: _description_
    :type models: _type_
    """

    class SyncModeChoices(models.TextChoices):
        SYNC_FULL = "f", _("full")
        SYNC_INCREMENTAL = "i", _("incremental")

    class SyncPolicyChoices(models.TextChoices):
        UPDATE_SOURCE = "s", _("source")
        UPDATE_TARGET = "t", _("target")
        UPDATE_MERGE = "m", _("merge")

    sync_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(
        Namespace,
        related_name="%(app_label)s_%(class)s_namespaces_related",
        related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="namespace of synchronisation",
    )
    name = models.TextField(blank=True, unique=True, help_text="name of the synchronisaton")

    server_target = models.ForeignKey(
        LARAServer,
        related_name="%(app_label)s_%(class)s_server_related",
        related_query_name="%(app_label)s_%(class)s_server_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="LARA server to be synchronised with",
    )

    datetime_start = models.DateTimeField(default=timezone.now, help_text="synchronisation start")
    datetime_end = models.DateTimeField(
        default=datetime.datetime(2066, 12, 31, tzinfo=datetime.timezone.utc), help_text="synchronisation end"
    )
    mode = models.CharField(
        max_length=1,
        choices=SyncModeChoices.choices,
        default=SyncModeChoices.SYNC_FULL,
        help_text="synchronisation mode: full, incremental",
    )
    update_policy = models.CharField(
        max_length=1,
        choices=SyncPolicyChoices.choices,
        default=SyncPolicyChoices.UPDATE_MERGE,
        help_text="update policy: source, target, merge",
    )
    event_repeat = models.TextField(
        blank=True,
        null=True,
        help_text="apache airflow ('@daily', '@weekly') or cron expression like entry: '12 23 * * *' :  every day at 23:12h do this task",
    )
    event_repeat_JSON = models.JSONField(
        blank=True, null=True, help_text="advanced repeat expressions possible, e.g. for uneaven orrurences"
    )
    logs = models.JSONField(null=True, blank=True, help_text="logs of the synchronisation")
    status = models.ForeignKey(
        ItemStatus,
        related_name="%(app_label)s_%(class)s_status_related",
        related_query_name="%(app_label)s_%(class)s_status_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="synchronisation status classifier: planned, started, in_progress, finished",
    )
    log_file = models.FileField(
        upload_to="projects/sync", blank=True, null=True, help_text="log file with rel. path/filename"
    )
    description = models.TextField(blank=True, null=True, help_text="description of extra data type")

    def __str__(self):
        return self.name or ""

    def __repr__(self) -> str:
        return self.name or ""

    # duration will be calculated

    class Meta:
        abstract = True


class CalendarAbstract(models.Model):
    """_summary_
    see https://www.ietf.org/rfc/rfc2445.txt
        RFC 5545

    see also: https://pypi.org/project/ics/
         and  https://pypi.org/project/icalendar/

    :param models: _description_
    :type models: _type_
    """

    title = models.TextField(help_text="title of the calendar entry")
    calendar_URL = models.URLField(
        blank=True, help_text="Universal Resource Locator (URL), to google calendar, iCalendar, .... "
    )
    summary = models.TextField(blank=True, help_text="short summary text of the calendar entry")
    description = models.TextField(blank=True, help_text="text of the calendar entry")
    color = models.TextField(
        default=ColorBlind16.full_color_scheme[randint(0, 16)].get_web(),
        blank=True,
        help_text="text of the calendar entry",
    )
    # organizer, cn, attendees
    # user - needs to be defined in the instantiation
    ics = models.TextField(blank=True, help_text="iCalendar entry in (RFC 5545) file format")
    datetime_start = models.DateTimeField(
        default=timezone.now, help_text="start date & time of the event. RFC 5545 - DTSTART"
    )
    datetime_end = models.DateTimeField(default=timezone.now, help_text="end datetime of the event. RFC 5545 - DTEND")
    duration = models.TextField(
        blank=True,
        help_text=""" This value type is used to identify properties that contain
         a duration of time. RFC 5545 - DURATION""",
    )
    all_day = models.BooleanField(default=False, help_text="All day event ? (boolean)")

    created = models.DateTimeField(default=timezone.now, help_text="end datetime of the event")
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when calendar entry was last modified"
    )
    timestamp = models.DateTimeField(default=timezone.now, help_text="timestamp of the event")
    # transparency
    location = models.ForeignKey(
        Location,
        related_name="%(app_label)s_%(class)s_location_related",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="location",
    )
    geolocation = models.ForeignKey(
        GeoLocation,
        related_name="%(app_label)s_%(class)s_geolocation_related",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="GEO location",
    )

    conference_URL = models.URLField(
        blank=True,
        help_text="audio/video conference/meeting Universal Resource Locator (URL), e.g. zoom, jitsi, meet, ...",
    )

    tags = models.ManyToManyField(
        Tag,
        related_name="%(app_label)s_%(class)s_tags_related",
        related_query_name="%(app_label)s_%(class)s_tags_related_query",
        blank=True,
        help_text="tags of calendar entry",
    )
    # many parameters / fields of the RFC 5545 should be stored in the ics field
    # participation status
    range = models.TextField(
        blank=True,
        help_text="""To specify the effective range of recurrence instances from
        the instance specified by the recurrence identifier specified by
        the property. RFC 5545 - RANGE """,
    )
    related = models.TextField(
        blank=True,
        help_text="""To specify the relationship of the alarm trigger with
        respect to the start or end of the calendar component. RFC 5545 - RELATED""",
    )
    role = models.TextField(
        blank=True,
        help_text="""To specify the participation role for the calendar user
        specified by the property. RFC 5545 - ROLE""",
    )
    tzid = models.TextField(
        blank=True,
        help_text="""To specify the identifier for the time zone definition for
        a time component in the property value.
        RFC 5545 - TZID""",
    )
    offset_UTC = models.IntegerField(
        blank=True,
        default=0,
        help_text=""" This value type is used to identify properties that contain
      an offset from UTC to local time (+ or - num. hours) . RFC 5545 -  UTC-OFFSET""",
    )
    # alarm_method - mail ...
    alarm_repeat_JSON = models.JSONField(
        blank=True, null=True, help_text="advanced alarm repeat expressions possible, e.g. for uneaven orrurences"
    )
    event_repeat = models.TextField(
        blank=True,
        help_text="apache airflow ('@daily', '@weekly') or cron expresion like entry: '12 23 * * *' :  every day at 23:12h do this task",
    )
    event_repeat_JSON = models.JSONField(
        blank=True, null=True, help_text="advanced event repeat expressions possible, e.g. for uneaven orrurences"
    )

    # autogenerate ICS on save, if ics is empty.
    # see: https://icalendar.readthedocs.io
    # def save(self, *args, **kwargs):
    #     if self.entity.email == "":
    #         self.entity.email = self.user.email

    #     if self.user.first_name == "":
    #         self.user.first_name = self.entity.name_first

    #     if self.user.last_name == "":
    #         self.user.last_name = self.entity.name_last

    #     if self.user.email == "":
    #         self.user.email = self.entity.email

    #     self.user.save()

    #     super().save(*args, **kwargs)

    def __str__(self):
        return self.title or ""

    def __repr__(self):
        return self.title or ""

    class Meta:
        abstract = True
