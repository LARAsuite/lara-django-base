"""
________________________________________________________________________

:PROJECT: LARA

* LARA colour schemes*

:details:  colour schemes - 
            see: https://github.com/vaab/colour
            .

:author:  mark doerr <mark.doerr@uni.greifswald.de> 
          
:date: (creation)          20200522

.. note:: some remarks
.. todo:: - 
________________________________________________________________________

"""

__version__ = "v0.0.3"


from colour import Color

# def changeColorSaturation(color: str="greed", factor: int = 0):
#     return Color(color).lighter(factor).name(Color.HexRgb)


class ColorBlind4:
    """ source: https://davidmathlogic.com/colorblind/ 
    """
    magenta = "#D81B60"
    light_blue = "#1E88E5"
    light_orange = "#FFC107"
    dark_green = "#004D40"

    @property
    def full_scheme(self):
        return [self.magenta, self.light_blue, self.light_orange, self.dark_green]

    @property
    def magenta(self):
        return Color(self.magenta)

    @property
    def light_blue(self):
        return Color(self.light_blue)

    @property
    def light_orange(self):
        return Color(self.light_orange)

    @property
    def dark_green(self):
        return Color(self.dark_green)

    @property
    def full_color_scheme(self):
        return [Color(col) for col in self.full_scheme]

    # @property
    # def full_scheme_lighter(self, factor: int = 0):
    #     return [col.lighter(factor) for col in self.full_color_scheme]

    # @classmethod
    # def lighter_color(cls, color: Color = None, factor: int = 0):
    #     return color.lighter(factor)

    # @classmethod
    # def lighter_color_str(cls, color: Color = None, factor: int = 0):
    #     q_col = color.lighter(factor)
    #     return q_col.name(Color.HexRgb)


class ColorBlind8:
    """
        # 8- pfor deuteranopia  bs
        # 18:05:14 Tue 19 May 2020
        # Martin Krzywinski martink@bcgsc.ca
        # Methods and details: http://mkweb.bcgsc.ca/colorblind
        # Color names: http://mkweb.bcgsc.ca/colornames
        # Main palette colors are N-main with N-alt being (one of many) alternatives
        # that are indistinguishable to deuteranopes.
    """
    black = "#000000"  # 0   0   0 black, grey, grey, grey, rich black, grey, cod grey, grey, almost black, grey
    sapphire = "#2271B2"  # 34 113 178 honolulu blue, bluish, strong cornflower blue, spanish blue, medium persian blue, sapphire blue, ocean boat blue, french blue, windows blue, tufts blue
    purple = "#AA0DB4"  # 170  13 180 barney, strong magenta, heliotrope magenta, strong heliotrope, steel pink, barney purple, purple, violet, violet eggplant, deep magenta
    summer_sky = "#3DB7E9"  # 61 183 233 summer sky, cyan, picton blue, vivid cerulean, deep sky blue, brilliant cornflower blue, malibu, bright cerulean, cerulean, cerulean
    candy_pink = "#FF54ED"  # 255  84 237 light magenta, violet pink, light brilliant magenta, pink flamingo, light brilliant orchid, brilliant magenta, purple pizzazz, candy pink, blush pink, shocking pink
    wild_strawberry = "#F748A5"  # 247  72 165 barbie pink, rose bonbon, wild strawberry, brilliant rose, brilliant rose, magenta, wild strawberry, light brilliant rose, frostbite, brilliant cerise
    topaz = "#00B19F"  # 0 177 159 strong opal, tealish, persian green, keppel, topaz, manganese blue, light sea green, sea green light, puerto rico, turquoise
    ocean_green = "#359B73"  # 53 155 115 ocean green, sea green, viridian, mother earth, moderate spring green, moderate aquamarine, paolo veronese green, observatory, jungle green, ocean green
    vivid_rose = "#EB057A"  # 235   5 122 vivid rose, red purple, mexican pink, bright pink, rose, strong pink, luminous vivid rose, deep pink, winter sky, hot pink
    bamboo = "#d55e00"  # 213  94   0 bamboo, smoke tree, red stage, tawny, tenn, tenne, burnt orange, rusty orange, dark orange, mars yellow
    cherry_red = "#F8071D"  # 248   7  29 vivid red, luminous vivid amaranth, ruddy, ku crimson, vivid amaranth, light brilliant red, cherry red, red, red, bright red
    sun = "#e69f00"  # 230 159   0 gamboge, squash, buttercup, marigold, dark goldenrod, medium goldenrod, fuel yellow, sun, harvest gold, orange
    dark_orange = "#FF8D1A"  # 255 141  26 dark orange, juicy, west side, tangerine, gold drop, pizazz, princeton orange, university of tennessee orange, tangerine, tahiti gold
    # 240 228  66 holiday, buzz, paris daisy, starship, golden fizz, dandelion, gorse, lemon yellow, bright lights, sunflower
    lemon_yellow = "#f0e442"
    lime = "#9EFF37"  # 158 255  55 french lime, lime, green yellow, green lizard, luminous vivid spring bud, spring frost, vivid spring bud, bright yellow green, spring bud, acid green

    full_scheme = [black, sapphire, purple, summer_sky,
                   candy_pink, wild_strawberry, topaz,
                   ocean_green, vivid_rose, bamboo, cherry_red, sun, dark_orange, lemon_yellow, lime]

    # @property # wrong syntax !
    def full_color_scheme(self):
        return [Color(col) for col in self.full_scheme]

    # #@property wrong syntax
    # def full_scheme_lighter(self, factor: int = 30):
    #     return [col.lighter(factor) for col in self.full_color_scheme]

    # @classmethod
    # def lighter_qcolor(cls, color: Color = None, factor: int = 0):
    #     return color.lighter(factor)

    # @classmethod
    # def lighter_color_str(cls, color: Color = None, factor: int = 0):
    #     q_col = color.lighter(factor)
    #     return q_col.name(Color.HexRgb)


class ColorBlind16:
    """
        # 8- pfor deuteranopia  bs
        # 18:05:14 Tue 19 May 2020
        # Martin Krzywinski martink@bcgsc.ca
        # Methods and details: http://mkweb.bcgsc.ca/colorblind
        # Color names: http://mkweb.bcgsc.ca/colornames
        # Main palette colors are N-main with N-alt being (one of many) alternatives
        # that are indistinguishable to deuteranopes.
    """
    black = "#000000"  # 0   0   0 black, grey, grey, grey, rich black, grey, cod grey, grey, almost black, grey
    sapphire = "#2271B2"  # 34 113 178 honolulu blue, bluish, strong cornflower blue, spanish blue, medium persian blue, sapphire blue, ocean boat blue, french blue, windows blue, tufts blue
    purple = "#AA0DB4"  # 170  13 180 barney, strong magenta, heliotrope magenta, strong heliotrope, steel pink, barney purple, purple, violet, violet eggplant, deep magenta
    summer_sky = "#3DB7E9"  # 61 183 233 summer sky, cyan, picton blue, vivid cerulean, deep sky blue, brilliant cornflower blue, malibu, bright cerulean, cerulean, cerulean
    candy_pink = "#FF54ED"  # 255  84 237 light magenta, violet pink, light brilliant magenta, pink flamingo, light brilliant orchid, brilliant magenta, purple pizzazz, candy pink, blush pink, shocking pink
    wild_strawberry = "#F748A5"  # 247  72 165 barbie pink, rose bonbon, wild strawberry, brilliant rose, brilliant rose, magenta, wild strawberry, light brilliant rose, frostbite, brilliant cerise
    topaz = "#00B19F"  # 0 177 159 strong opal, tealish, persian green, keppel, topaz, manganese blue, light sea green, sea green light, puerto rico, turquoise
    ocean_green = "#359B73"  # 53 155 115 ocean green, sea green, viridian, mother earth, moderate spring green, moderate aquamarine, paolo veronese green, observatory, jungle green, ocean green
    vivid_rose = "#EB057A"  # 235   5 122 vivid rose, red purple, mexican pink, bright pink, rose, strong pink, luminous vivid rose, deep pink, winter sky, hot pink
    bamboo = "#d55e00"  # 213  94   0 bamboo, smoke tree, red stage, tawny, tenn, tenne, burnt orange, rusty orange, dark orange, mars yellow
    cherry_red = "#F8071D"  # 248   7  29 vivid red, luminous vivid amaranth, ruddy, ku crimson, vivid amaranth, light brilliant red, cherry red, red, red, bright red
    sun = "#e69f00"  # 230 159   0 gamboge, squash, buttercup, marigold, dark goldenrod, medium goldenrod, fuel yellow, sun, harvest gold, orange
    dark_orange = "#FF8D1A"  # 255 141  26 dark orange, juicy, west side, tangerine, gold drop, pizazz, princeton orange, university of tennessee orange, tangerine, tahiti gold
    # 240 228  66 holiday, buzz, paris daisy, starship, golden fizz, dandelion, gorse, lemon yellow, bright lights, sunflower
    lemon_yellow = "#f0e442"
    lime = "#9EFF37"  # 158 255  55 french lime, lime, green yellow, green lizard, luminous vivid spring bud, spring frost, vivid spring bud, bright yellow green, spring bud, acid green

    scheme8_str = [black, sapphire, purple, summer_sky,
                   candy_pink, wild_strawberry, topaz,
                   ocean_green, vivid_rose, bamboo, cherry_red, sun, dark_orange, lemon_yellow, lime]

    scheme8_color = [Color(col) for col in scheme8_str]

    # col.set_saturation(0.8)
    scheme8_light_color = [col for col in scheme8_color]

    full_color_scheme = scheme8_color + scheme8_light_color

    # @classmethod
    # def lighter_color(cls, color: Color = None, factor: float = 0.0):

    #     return color.set_saturation(factor)

    # @property # wrong syntax !
    # def full_color_scheme(self):
    #     return  full_color_scheme
