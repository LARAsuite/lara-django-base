"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base app *

:details: lara_django_base app configuration. 
         This provides a generic django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/4.0/ref/applications/
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

import os
from django.conf import settings
from django.apps import AppConfig
from SPARQLWrapper import SPARQLWrapper, JSON, RDF, CSV, TURTLE, DIGEST

from emmopy import get_emmo
from ontopy import World
from rdflib import *


class LaraDjangoBaseConfig(AppConfig):
    name = 'lara_django_base'
    default_auto_field = 'django.db.models.BigAutoField'
    # enter a verbose name for your app: lara_django_base here - this will be used in the admin interface
    verbose_name = 'LARA-django Base'
    # lara_app_icon = 'lara_django_base_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.
    lara_app_icon = 'LARA_logo.svg'
    
    # sparql = SPARQLWrapper(f'http://{settings.VIRTUOSO_HOST}:{settings.VIRTUOSO_ADMIN_PORT}/sparql-auth/')
    # sparql.setHTTPAuth(DIGEST)
    # sparql.setCredentials(settings.VIRTUOSO_USER, settings.VIRTUOSO_PASSWORD)
    # sparql.setReturnFormat(JSON)
    # def __init__(self, app_name, app_module):
    #     super().__init__(app_name, app_module)

    def ready(self):
        
        self.sparql = SPARQLWrapper(f'http://{settings.VIRTUOSO_HOST}:{settings.VIRTUOSO_ADMIN_PORT}/sparql-auth/')
        self.sparql.setHTTPAuth(DIGEST)
        self.sparql.setCredentials(settings.VIRTUOSO_USER, settings.VIRTUOSO_PASSWORD)
        self.sparql.setReturnFormat(JSON)

        # loading the EMMO ontology

        if settings.ONTOLOGY_PATH:
            oso_data_url = os.path.join(settings.ONTOLOGY_PATH, "oso_data.ttl")
        else:
            #oso_data_filename = "../ontologies/oso_data.ttl"
            oso_data_url = "https://gitlab.com/opensourcelab/scientificdata/ontologies/openscienceontology/data/-/raw/main/ontologies/oso_data.ttl"

        self.emmo_world = World()
        self.emmo = self.emmo_world.get_ontology(oso_data_url)
        self.emmo.load()               # reload_if_newer = True
        self.emmo.sync_python_names()  # synchronize annotations

        self.emmo_graph = self.emmo_world.as_rdflib_graph()
        emmo_iri = 'http://emmo.info/emmo-inferred#'
        abox_iri = 'http://www.labop.org/labop_labware_abox#'
        tbox_iri = 'http://www.labop.org/labop_labware_tbox#'
        self.emmo_graph.bind('emmo', emmo_iri)
        self.emmo_graph.bind('tbox', tbox_iri)
        self.emmo_graph.bind('abox', abox_iri)
        self.emmo_graph.bind("owl", "http://www.w3.org/2002/07/owl#")


        LARA_welcome_txt = ("_____________________________________________\n\n"
                         "  Welcome to                                 \n"
                         "   __      __    ____    __                  \n"
                         "  (  )    /__\  (  _ \  /__\                 \n"
                         "   )(__  /(__)\  )   / /(__)\                \n"
                         "  (____)(__)(__)(_)\_)(__)(__)               \n"
                         "                                             \n"
                         "   To access LARA go to:                     \n"
                         f"     https://{settings.LARA_HOSTNAME}:{settings.LARA_PORT}\n"
                         "        \n"
                         "_____________________________________________\n\n")

        print(LARA_welcome_txt)
