"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data filter app *

:details: lara_django_data filter app. 
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


from django_filters.rest_framework import FilterSet, CharFilter, DateRangeFilter

from .models import (
    Namespace,
    MediaType,
    ExtraData,
    ExternalResource,
    Tag,
    ItemStatus,
    DataType,
    PhysicalUnit,
    PhysicalStateMatter,
    ScientificConstant,
    Currency,
    GeoLocation,
    Country,
    CountrySubdivision,
    City,
    Address,
    RoomCategory,
    Room,
    Location,
)


class MediaTypeFilterSet(FilterSet):
    class Meta:
        model = MediaType
        fields = {
            "name": ["exact", "contains"],
            "media_type": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class DataTypeFilterSet(FilterSet):
    class Meta:
        model = DataType
        fields = {
            "data_type": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class NamespaceFilterSet(FilterSet):
    # URI = CharFilter(field_name="URI", lookup_expr="contains")
    class Meta:
        model = Namespace
        fields = {
            "URI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class TagFilterSet(FilterSet):
    class Meta:
        model = Tag
        fields = {
            "tag": ["exact", "contains"],
            "IRI": ["exact", "contains"],
        }


class ItemStatusFilterSet(FilterSet):
    class Meta:
        model = ItemStatus
        fields = {
            "status": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class ExtraDataFilterSet(FilterSet):
    class Meta:
        model = ExtraData
        fields = {
            "title": ["exact", "contains"],
            "name": ["exact", "contains"],
            "name_full": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "URL": ["exact", "contains"],
            "datetime_created": ["lt", "gt", "exact", "range"],
            "datetime_last_modified": ["lt", "gt", "exact", "range"],
            "description": ["exact", "contains"],
        }


class ExternalResourceFilterSet(FilterSet):
    """This filter is used to filter ExternalResource objects by name, description and URL."""

    class Meta:
        model = ExternalResource
        fields = {
            "name": ["exact", "contains"],
            "title": ["exact", "contains"],
            "URL": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
            "datetime_last_modified": ["lt", "gt", "exact", "range"],
        }


class PhysicalUnitFilterSet(FilterSet):
    class Meta:
        model = PhysicalUnit
        fields = {
            "name": ["exact", "contains"],
            "symbol": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class ScientificConstantFilterSet(FilterSet):
    class Meta:
        model = ScientificConstant
        fields = {
            "name": ["exact", "contains"],
            "symbol": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "URL_source": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class CurrencyFilterSet(FilterSet):
    class Meta:
        model = Currency
        fields = {
            "name": ["exact", "contains"],
            "symbol": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "datetime_last_modified": ["lt", "gt", "exact", "range"],
        }


class GeoLocationFilterSet(FilterSet):
    class Meta:
        model = GeoLocation
        fields = {
            "name": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "datetime_last_modified": ["lt", "gt", "exact", "range"],
        }


class CountryFilterSet(FilterSet):
    class Meta:
        model = Country
        fields = {
            "name": ["exact", "contains"],
            "name_local": ["exact", "contains"],
            "alpha2code": ["exact", "contains"],
            "alpha3code": ["exact", "contains"],
            "numeric_code": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class CountrySubdivisionFilterSet(FilterSet):
    class Meta:
        model = CountrySubdivision
        fields = {
            "name": ["exact", "contains"],
            "name_local": ["exact", "contains"],
            "code": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class CityFilterSet(FilterSet):
    class Meta:
        model = City
        fields = {
            "name": ["exact", "contains"],
            "name_local": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class AddressFilterSet(FilterSet):
    class Meta:
        model = Address
        fields = {
            "building": ["exact", "contains"],
            "street": ["exact", "contains"],
            "house_number": ["exact", "contains"],
            "supplement": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class RoomCategoryFilterSet(FilterSet):
    class Meta:
        model = RoomCategory
        fields = {
            "category": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class RoomFilterSet(FilterSet):
    class Meta:
        model = Room
        fields = {
            "name": ["exact", "contains"],
            "number": ["exact", "contains"],
            "floor": ["exact", "contains"],
            "phone_number": ["exact", "contains"],
            "area": ["exact", "contains"],
            "max_people": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class LocationFilterSet(FilterSet):
    class Meta:
        model = Location
        fields = {
            "name": ["exact", "contains"],
            "barcode1D": ["exact", "contains"],
            "cabinet": ["exact", "contains"],
            "shelf": ["exact", "contains"],
            "drawer": ["exact", "contains"],
            "box": ["exact", "contains"],
            "IRI": ["exact", "contains"],
            "description": ["exact", "contains"],
        }
