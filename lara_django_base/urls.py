"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base urls *

:details: lara_django_base urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
from django_socio_grpc.settings import grpc_settings

from . import views

# !! this sets the apps namespace to be used in the template
app_name = "lara_django_base"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view

urlpatterns = [
    path('', views.HomeMenu.as_view(), name='index'),
    path('apps/', views.AppsMenu.as_view(), name='apps'),
    path('about/', views.AboutView.as_view(), name='base-about'),
    path('addresses/list/', views.AddressesListView.as_view(), name='address-list'),
    path('addresses/create/', views.AddressCreateView.as_view(),
         name='address-create'),
    path('addresses/update/<uuid:pk>', views.AddressUpdateView.as_view(),
         name='address-update'),
    path('addresses/delete/<uuid:pk>', views.AddressDeleteView.as_view(),
         name='address-delete'),
    path('addresses/<uuid:pk>/', views.AddressDetailView.as_view(),
         name='address-details'),
    # path('subdir/', include('.urls')),
]

grpc_settings.user_settings["GRPC_HANDLERS"] += [
   "lara_django_base.grpc.handlers.grpc_handlers"]
