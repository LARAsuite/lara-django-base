#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Thanks to  Pina Merkert (pmk@ct.de, https://github.com/pinae/Nutria-DB )

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        if User.objects.count() == 0:
            new_admin = User.objects.create_superuser(
                username=os.getenv('DJANGO_SUPERUSER_USERNAME', 'admin'),
                email=os.getenv('DJANGO_SUPERUSER_MAIL',
                                'admin@example.com'),
                password=os.getenv('DJANGO_SUPERUSER_PASSWORD', 'yxcv4321'))
            new_admin.save()
            #print(f"Django Admin/Superuser account created with username: {os.getenv('DJANGO_SUPERUSER', 'admin')} and pw: {os.getenv('DJANGO_SUPERUSER_PASSWORD', 'yxcv4321')}")
            print(f"Django Admin/Superuser account created")
        else:
            print('Admin accounts can only be initialized if no Accounts exist')
