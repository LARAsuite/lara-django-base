from django.conf import settings


def external_services_urls(request):
    return {
        "LARA_DOCS_URL": settings.LARA_DOCS_URL,
        "PREFECT_DASHBOARD_URL" : settings.PREFECT_DASHBOARD_URL,
        "VIRTUOSO_SPARQL_URL": settings.VIRTUOSO_SPARQL_URL,
        "MINIO_DASHBOARD" : settings.MINIO_DASHBOARD,
    }