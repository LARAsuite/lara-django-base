"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_base admin *

:details: lara_django_base admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_base > forms.py" to update this file
________________________________________________________________________
"""


from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, HTML

# from crispy_forms.bootstrap import FormActions


from .models import (
    DataType,
    MediaType,
    Namespace,
    Tag,
    ItemStatus,
    ExtraData,
    PhysicalUnit,
    ScientificConstant,
    Currency,
    GeoLocation,
    Country,
    SubdivisionType,
    CountrySubdivision,
    City,
    Address,
    RoomCategory,
    Room,
    Location,
)


class DataTypeCreateForm(forms.ModelForm):
    class Meta:
        model = DataType
        fields = ("data_type", "IRI", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("data_type", "IRI", "description", Submit("submit", "Create"))


class DataTypeUpdateForm(forms.ModelForm):
    class Meta:
        model = DataType
        fields = ("data_type", "IRI", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("data_type", "IRI", "description", Submit("submit", "Create"))


class MediaTypeCreateForm(forms.ModelForm):
    class Meta:
        model = MediaType
        fields = ("name", "media_type", "def_filename_extension_regex", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name", "media_type", "def_filename_extension_regex", "description", Submit("submit", "Create")
        )


class MediaTypeUpdateForm(forms.ModelForm):
    class Meta:
        model = MediaType
        fields = ("name", "media_type", "def_filename_extension_regex", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name", "media_type", "def_filename_extension_regex", "description", Submit("submit", "Create")
        )


class NamespaceCreateForm(forms.ModelForm):
    class Meta:
        model = Namespace
        fields = ("URI","description",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("description", Submit("submit", "Create"))


class NamespaceUpdateForm(forms.ModelForm):
    class Meta:
        model = Namespace
        fields =  ("URI","description",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("description", Submit("submit", "Create"))


class TagCreateForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ("tag",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("tag", Submit("submit", "Create"))


class TagUpdateForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ("tag",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("tag", Submit("submit", "Create"))


class ItemStatusCreateForm(forms.ModelForm):
    class Meta:
        model = ItemStatus
        fields = ("status", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("status", "description", Submit("submit", "Create"))


class ItemStatusUpdateForm(forms.ModelForm):
    class Meta:
        model = ItemStatus
        fields = ("status", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("status", "description", Submit("submit", "Create"))


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = ("data_type", "namespace", "data_JSON", "media_type", "IRI", "URL", "description", "file", "image")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "data_type",
            "namespace",
            "data_JSON",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
            Submit("submit", "Create"),
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = ("data_type", "namespace", "data_JSON", "media_type", "IRI", "URL", "description", "file", "image")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "data_type",
            "namespace",
            "data_JSON",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
            Submit("submit", "Create"),
        )


class PhysicalUnitCreateForm(forms.ModelForm):
    class Meta:
        model = PhysicalUnit
        fields = ("name", "dimension", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "dimension", "description", Submit("submit", "Create"))


class PhysicalUnitUpdateForm(forms.ModelForm):
    class Meta:
        model = PhysicalUnit
        fields = ("name", "dimension", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "dimension", "description", Submit("submit", "Create"))


class ScientificConstantCreateForm(forms.ModelForm):
    class Meta:
        model = ScientificConstant
        fields = ("name", "value", "unit", "data_JSON", "IRI", "URL_source", "URL_definition", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "value",
            "unit",
            "data_JSON",
            "IRI",
            "URL_source",
            "URL_definition",
            "description",
            Submit("submit", "Create"),
        )


class ScientificConstantUpdateForm(forms.ModelForm):
    class Meta:
        model = ScientificConstant
        fields = ("name", "value", "unit", "data_JSON", "IRI", "URL_source", "URL_definition", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "value",
            "unit",
            "data_JSON",
            "IRI",
            "URL_source",
            "URL_definition",
            "description",
            Submit("submit", "Create"),
        )


class CurrencyCreateForm(forms.ModelForm):
    class Meta:
        model = Currency
        fields = ("name", "IRI", "alpha3code", "numeric_code", "symbol", "exchange_rate_EUR")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name", "IRI", "alpha3code", "numeric_code", "symbol", "exchange_rate_EUR", Submit("submit", "Create")
        )


class CurrencyUpdateForm(forms.ModelForm):
    class Meta:
        model = Currency
        fields = ("name", "IRI", "alpha3code", "numeric_code", "symbol", "exchange_rate_EUR")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name", "IRI", "alpha3code", "numeric_code", "symbol", "exchange_rate_EUR", Submit("submit", "Create")
        )


class GeoLocationCreateForm(forms.ModelForm):
    class Meta:
        model = GeoLocation
        fields = ("name", "coordinates_DD_lat", "coordinates_DD_long", "elevation", "openstreetmap", "googlemap")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "coordinates_DD_lat",
            "coordinates_DD_long",
            "elevation",
            "openstreetmap",
            "googlemap",
            Submit("submit", "Create"),
        )


class GeoLocationUpdateForm(forms.ModelForm):
    class Meta:
        model = GeoLocation
        fields = ("name", "coordinates_DD_lat", "coordinates_DD_long", "elevation", "openstreetmap", "googlemap")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "coordinates_DD_lat",
            "coordinates_DD_long",
            "elevation",
            "openstreetmap",
            "googlemap",
            Submit("submit", "Create"),
        )


class CountryCreateForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = ("name", "name_local", "alpha2code", "alpha3code", "numeric_code", "phone_code", "icon", "currency")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_local",
            "alpha2code",
            "alpha3code",
            "numeric_code",
            "phone_code",
            "icon",
            "currency",
            Submit("submit", "Create"),
        )


class CountryUpdateForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = ("name", "name_local", "alpha2code", "alpha3code", "numeric_code", "phone_code", "icon", "currency")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_local",
            "alpha2code",
            "alpha3code",
            "numeric_code",
            "phone_code",
            "icon",
            "currency",
            Submit("submit", "Create"),
        )


class SubdivisionTypeCreateForm(forms.ModelForm):
    class Meta:
        model = SubdivisionType
        fields = ("name", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "description", Submit("submit", "Create"))


class SubdivisionTypeUpdateForm(forms.ModelForm):
    class Meta:
        model = SubdivisionType
        fields = ("name", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "description", Submit("submit", "Create"))


class CountrySubdivisionCreateForm(forms.ModelForm):
    class Meta:
        model = CountrySubdivision
        fields = ("name", "name_local", "code", "alpha2code", "subdivision_type", "country", "timezone")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_local",
            "code",
            "alpha2code",
            "subdivision_type",
            "country",
            "timezone",
            Submit("submit", "Create"),
        )


class CountrySubdivisionUpdateForm(forms.ModelForm):
    class Meta:
        model = CountrySubdivision
        fields = ("name", "name_local", "code", "alpha2code", "subdivision_type", "country", "timezone")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_local",
            "code",
            "alpha2code",
            "subdivision_type",
            "country",
            "timezone",
            Submit("submit", "Create"),
        )


class CityCreateForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ("name", "name_local", "country", "subdivision", "timezone")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name", "name_local", "country", "subdivision", "timezone", Submit("submit", "Create")
        )


class CityUpdateForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ("name", "name_local", "country", "subdivision", "timezone")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name", "name_local", "country", "subdivision", "timezone", Submit("submit", "Create")
        )


class AddressCreateForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = (
            "building",
            "street",
            "house_number",
            "supplement",
            "city",
            "subdivision",
            "country",
            "postal_code",
            "geolocation",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "building",
            "street",
            "house_number",
            "supplement",
            "city",
            "subdivision",
            "country",
            "postal_code",
            "geolocation",
            Submit("submit", "Create"),
        )


class AddressUpdateForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = (
            "building",
            "street",
            "house_number",
            "supplement",
            "city",
            "subdivision",
            "country",
            "postal_code",
            "geolocation",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "building",
            "street",
            "house_number",
            "supplement",
            "city",
            "subdivision",
            "country",
            "postal_code",
            "geolocation",
            Submit("submit", "Create"),
        )


# class AddressCreateForm(forms.ModelForm):
#     class Meta:
#         model = Address
#         fields = ('building',
#                   'street',
#                   'house_number',
#                   'supplement',
#                   'city',
#                   'subdivision',
#                   'country',
#                   'postal_code',
#                   'geolocation',
#                   'data_extra')

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.helper = FormHelper()
#         self.helper.layout = Layout('building',
#                                     Row(
#                                         Column(
#                                             'street', css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'house_number', css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'supplement', css_class='form-group col-md-6 mb-0'),
#                                         css_class='form-row'
#                                     ),
#                                     Row(
#                                         Column(
#                                             'postal_code',  css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'city', css_class='form-group col-md-6 mb-0'),
#                                         css_class='form-row'
#                                     ),
#                                     Row(
#                                         Column(
#                                             'subdivision',  css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'country', css_class='form-group col-md-6 mb-0'),
#                                         css_class='form-row'
#                                     ),
#                                     'geolocation',
#                                     'data_extra',
#                                     Submit(
#                                         'submit', 'Create'),
#                                     HTML(
#                                         '<a class="btn btn-danger" href="/people/addresses/list">Cancel</a>')
#                                     # Button('cancel', 'Cancel',
#                                     #        css_class='btn btn-danger')
#                                     )


# class AddressUpdateForm(forms.ModelForm):
#     class Meta:
#         model = Address
#         fields = ('building',
#                   'street',
#                   'house_number',
#                   'supplement',
#                   'city',
#                   'subdivision',
#                   'country',
#                   'postal_code',
#                   'geolocation',
#                   'data_extra')

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.helper = FormHelper()
#         self.helper.layout = Layout('building',
#                                     Row(
#                                         Column(
#                                             'street', css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'house_number', css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'supplement', css_class='form-group col-md-6 mb-0'),
#                                         css_class='form-row'
#                                     ),
#                                     Row(
#                                         Column(
#                                             'postal_code',  css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'city', css_class='form-group col-md-6 mb-0'),
#                                         css_class='form-row'
#                                     ),
#                                     Row(
#                                         Column(
#                                             'subdivision',  css_class='form-group col-md-6 mb-0'),
#                                         Column(
#                                             'country', css_class='form-group col-md-6 mb-0'),
#                                         css_class='form-row'
#                                     ),
#                                     'geolocation',
#                                     'data_extra',
#                                     Submit(
#                                         'submit', 'Create')
#                                     )


class RoomCategoryCreateForm(forms.ModelForm):
    class Meta:
        model = RoomCategory
        fields = ("category", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("category", "description", Submit("submit", "Create"))


class RoomCategoryUpdateForm(forms.ModelForm):
    class Meta:
        model = RoomCategory
        fields = ("category", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("category", "description", Submit("submit", "Create"))


class RoomCreateForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = (
            "name",
            "category",
            "number",
            "floor",
            "phone_number",
            "address",
            "room_map",
            "max_people",
            "area",
            "volume",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "category",
            "number",
            "floor",
            "phone_number",
            "address",
            "room_map",
            "max_people",
            "area",
            "volume",
            Submit("submit", "Create"),
        )


class RoomUpdateForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = (
            "name",
            "category",
            "number",
            "floor",
            "phone_number",
            "address",
            "room_map",
            "max_people",
            "area",
            "volume",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "category",
            "number",
            "floor",
            "phone_number",
            "address",
            "room_map",
            "max_people",
            "area",
            "volume",
            Submit("submit", "Create"),
        )


class LocationCreateForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = (
            "name",
            "barcode1D",
            "geolocation",
            "address",
            "building",
            "floor",
            "room",
            "cabinet",
            "shelf",
            "drawer",
            "box",
            "position",
            "data_JSON",
            "IRI",
            "URL",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "barcode1D",
            "geolocation",
            "address",
            "building",
            "floor",
            "room",
            "cabinet",
            "shelf",
            "drawer",
            "box",
            "position",
            "data_JSON",
            "IRI",
            "URL",
            "description",
            Submit("submit", "Create"),
        )


class LocationUpdateForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = (
            "name",
            "barcode1D",
            "geolocation",
            "address",
            "building",
            "floor",
            "room",
            "cabinet",
            "shelf",
            "drawer",
            "box",
            "position",
            "data_JSON",
            "IRI",
            "URL",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "barcode1D",
            "geolocation",
            "address",
            "building",
            "floor",
            "room",
            "cabinet",
            "shelf",
            "drawer",
            "box",
            "position",
            "data_JSON",
            "IRI",
            "URL",
            "description",
            Submit("submit", "Create"),
        )


# from .forms import DataTypeCreateForm, MediaTypeCreateForm, NamespaceCreateForm, TagCreateForm, ItemStatusCreateForm, ExtraDataCreateForm, PhysicalUnitCreateForm, ScientificConstantCreateForm, CurrencyCreateForm, GeoLocationCreateForm, CountryCreateForm, SubdivisionTypeCreateForm, CountrySubdivisionCreateForm, CityCreateForm, AddressCreateForm, RoomCategoryCreateForm, RoomCreateForm, LocationCreateFormDataTypeUpdateForm, MediaTypeUpdateForm, NamespaceUpdateForm, TagUpdateForm, ItemStatusUpdateForm, ExtraDataUpdateForm, PhysicalUnitUpdateForm, ScientificConstantUpdateForm, CurrencyUpdateForm, GeoLocationUpdateForm, CountryUpdateForm, SubdivisionTypeUpdateForm, CountrySubdivisionUpdateForm, CityUpdateForm, AddressUpdateForm, RoomCategoryUpdateForm, RoomUpdateForm, LocationUpdateForm
